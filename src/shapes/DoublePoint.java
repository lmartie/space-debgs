package shapes;

import org.eclipse.swt.graphics.Point;

public class DoublePoint {

	public double x;
	public double y;
	
	public DoublePoint(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public Point asSWTPoint(){
		return new Point((int)x, (int)y);
	}
}
