package shapes;

import org.eclipse.swt.graphics.Point;

public class Triangle {

	//left
	private DoublePoint one;
	//top
	private DoublePoint two;
	//right
	private DoublePoint three;
	//center
	private DoublePoint center;
	//bottom of triangle
	private Line base;
	//
	private double rotation = 0;
	//
	private Line perpendicular;

	
	public Triangle(DoublePoint one, DoublePoint two, DoublePoint three){
		this.one = one;
		this.two = two;
		this.three = three;
		
		double centerX = (one.x + two.x + three.x) / 3;
		double centerY = (one.y + two.y + three.y) / 3;
		center = new DoublePoint(centerX, centerY);
		
		
		setBase(new Line(one,three));
		
		rotate(0);

	}
	
	public DoublePoint getOne() {
		return one;
	}
	public void setOne(DoublePoint one) {
		this.one = one;
		recalCenter();
		setBase(new Line(one,three));
	}
	public DoublePoint getTwo() {
		return two;
	}
	public void setTwo(DoublePoint two) {
		this.two = two;
		recalCenter();
		setBase(new Line(one,three));
	}
	public DoublePoint getThree() {
		return three;
	}
	public void setThree(DoublePoint three) {
		this.three = three;
		recalCenter();
		setBase(new Line(one,three));
	}
	
	public void recalCenter(){
		double centerX = (one.x + two.x + three.x) / 3;
		double centerY = (one.y + two.y + three.y) / 3;
		center = new DoublePoint(centerX, centerY);
	}
	
	public DoublePoint getCenter(){
		return center;
	}
	
	private void translateToOrigin(){
		double newOneX = one.x - center.x;
		double newOneY = one.y - center.y;
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = two.x - center.x;
		double newTwoY = two.y - center.y;
		two = new DoublePoint(newTwoX,newTwoY);
		
		double newThreeX = three.x - center.x;
		double newThreeY = three.y - center.y;
		three = new DoublePoint(newThreeX,newThreeY);
		
		recalCenter();
	}
	
	public void moveBy(DoublePoint point){
		moveBy(point.x,point.y);
	}
	
	public void moveBy(double originalX, double originalY){
		double newOneX = one.x + originalX;
		double newOneY = one.y + originalY;
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = two.x + originalX;
		double newTwoY = two.y + originalY;
		two = new DoublePoint(newTwoX,newTwoY);
		
		double newThreeX = three.x + originalX;
		double newThreeY = three.y + originalY;
		three = new DoublePoint(newThreeX,newThreeY);
		
		recalCenter();
		setBase(new Line(one,three));
	}
	
	public void rotate(double angle){
		
		angle = -Math.toRadians(angle);
		
		this.rotation = this.rotation+angle;
		if (rotation > 360) {
			rotation = rotation % 360;
		}
		if (rotation < -360) {
			rotation = rotation % -360;
		}
		
		
		double originalX = center.x;
		double originalY = center.y;
		
		translateToOrigin();
		
		
		double newOneX = ((one.x*Math.cos(angle)) - (one.y*Math.sin(angle)));
		double newOneY = ((one.x*Math.sin(angle)) + (one.y*Math.cos(angle)));
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = ((two.x*Math.cos(angle)) - (two.y*Math.sin(angle)));
		double newTwoY = ((two.x*Math.sin(angle)) + (two.y*Math.cos(angle)));
		two = new DoublePoint(newTwoX,newTwoY);
		
		double newThreeX = ((three.x*Math.cos(angle)) - (three.y*Math.sin(angle)));
		double newThreeY = ((three.x*Math.sin(angle)) + (three.y*Math.cos(angle)));
		three = new DoublePoint(newThreeX,newThreeY);
		
		System.out.println("Triangle rotate:"+newOneX+" "+newOneY+" "+newTwoX+" "+newTwoY+" "+newThreeX+" "+newThreeY);

		
		
		
		//get new perpendicular
		double newPerpX = ((two.x*Math.cos(Math.toRadians(-180))) - (two.y*Math.sin(Math.toRadians(-180))));
		double newPerpY = ((two.x*Math.sin(Math.toRadians(-180))) + (two.y*Math.cos(Math.toRadians(-180))));
		

		
		
		moveBy(originalX,originalY);
		
		
		setBase(new Line(one,three));
		
		perpendicular = new Line(base.getCenter(),new DoublePoint(newPerpX*5+center.x,newPerpY*5+center.y));

		
	}
	
	public Line getBase() {
		return base;
	}
	
	public Line getPerpendicular(){
		return perpendicular;
		
	}

	public void setBase(Line base) {
		this.base = base;
	}
	
}
