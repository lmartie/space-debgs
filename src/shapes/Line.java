package shapes;

import org.eclipse.swt.graphics.Point;

public class Line {

	private DoublePoint one;
	private DoublePoint two;
	
	private DoublePoint center;
	
	
	
	public Line(DoublePoint one, DoublePoint two){
		this.one = one;
		this.two = two;
		calcCenter();
	
	}
	
	public void calcCenter(){
		this.setCenter(new DoublePoint((two.x+one.x)/2,(two.y+one.y)/2));
	}
	
	public DoublePoint getOne() {
		return one;
	}
	
	public void setOne(DoublePoint one) {
		this.one = one;
		calcCenter();
	}
	
	public DoublePoint getTwo() {
		return two;
	}
	
	public void setTwo(DoublePoint two) {
		this.two = two;
		calcCenter();
	}
	
	private void translateToOrigin(){
		double newOneX = one.x - getCenter().x;
		double newOneY = one.y - getCenter().y;
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = two.x - getCenter().x;
		double newTwoY = two.y - getCenter().y;
		two = new DoublePoint(newTwoX,newTwoY);
		
	}
	
	private void translateBack(){
		double newOneX = one.x + getCenter().x;
		double newOneY = one.y + getCenter().y;
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = two.x + getCenter().x;
		double newTwoY = two.y + getCenter().y;
		two = new DoublePoint(newTwoX,newTwoY);
		
	}
	
	
	public void rotate(double angle){
		angle = -Math.toRadians(angle);
		translateToOrigin();
		
		double newOneX = (one.x*Math.cos(angle)) - (one.y*Math.sin(angle));
		double newOneY = (one.x*Math.sin(angle)) + (one.y*Math.cos(angle));
		one = new DoublePoint(newOneX,newOneY);
		
		double newTwoX = (two.x*Math.cos(angle)) - (two.y*Math.sin(angle));
		double newTwoY = (two.x*Math.sin(angle)) + (two.y*Math.cos(angle));
		two = new DoublePoint(newTwoX,newTwoY);
		
		translateBack();
		
	}
	
	public double getLength(){
		return Math.sqrt(Math.pow((getTwo().x - getOne().x),2)-Math.pow((getTwo().y - getOne().y),2));
	}

	public DoublePoint getCenter() {
		return center;
	}

	public void setCenter(DoublePoint center) {
		this.center = center;
	}

	public double getSlope() {
		if((getTwo().x - getOne().x) == 0)
			return 0;
		
		double num = getTwo().y - getOne().y;
		double dem = getTwo().x - getOne().x;
		double denomSlope = (num)/(dem) ;
		
		
		return denomSlope;
	}


	
}
