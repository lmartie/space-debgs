package imageview;

import org.eclipse.ui.IStartup;

import client.EventCollection;
import client.EventOrganizer;
import client.EventProcessor;
import client.Timer;

public class StartUp implements IStartup {

	@Override
	public void earlyStartup() {
		EventCollection.getInstance();
		EventOrganizer.getInstance();
		EventProcessor.getInstance();
		Timer.getInstance();
		
	}

}
