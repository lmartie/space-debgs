package imageview.views;

import events.EventFactory;
import events.external.ExternalEvent;
import events.external.GameServerListRefresh;
import events.internal.InternalEvent;
import gui.GamesTable;
import gui.LunarLanderVisual;
import gui.ObserverLabel;
import gui.msgDialog.ConfigureGameServerDialog;
import gui.msgDialog.CreateGameDialog;
import gui.msgDialog.SpeedAngleDialog;

import imageview.Activator;

import java.awt.Rectangle;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.part.*;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;

import bugs.BugGenerator;

import client.EventCollection;
import client.EventOrganizer;
import client.EventProcessor;
import client.ExternalEventCollection;
import client.GameModel;
import client.GameStarter;
import client.PeerCommunicator;
import client.Referee;
import client.Timer;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */

public class GameView extends ViewPart implements Observer {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "imageview.views.GameView";

	public static GamesTable gamesTable = null;

	private int time = 0;

	public static LunarLanderVisual llvMe = null;
	public static LunarLanderVisual llvYou = null;
	public static ObserverLabel speedLabel = null;
	public static ObserverLabel rotationLabel = null;
	public static ObserverLabel fuelLabel = null;
	public static ObserverLabel timeLabel = null;
	public static ObserverLabel turnLabel = null;
	public static ObserverLabel newBugLabel = null;
	public static ObserverLabel altitudeLabel = null;
	public static ObserverLabel actualSpeedLabel = null;
	public static ObserverLabel statusLabel = null;
	
	public static String[] games = {};

	Button joinButton;
	Button createButton;
	Button findButton;
	Button configure;


	/**
	 * The constructor.
	 */
	public GameView() {
		File whereAmI = new File(".");
		System.out.println(whereAmI.getAbsolutePath());
		// on creation of gui
		EventCollection.getInstance();
		ExternalEventCollection.getInstance();
		EventOrganizer.getInstance();
		EventProcessor.getInstance();
		client.Timer.getInstance().addObserver(EventOrganizer.getInstance());
		Timer.getInstance().addObserver(this);
		Timer.getInstance().start();
//		Job job = new Job("GameStarter Find Job") {
//		     protected IStatus run(IProgressMonitor monitor) {
//		    	 GameStarter.getInstance().run();
//					
//		           return Status.OK_STATUS;
//		        }
//		     };
//		  job.setPriority(Job.SHORT);
//		  job.schedule(); // start as soon as possible
		  
		  PeerCommunicator.getInstance();
		  
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {

		final Canvas canvas = new Canvas(parent, SWT.None);
		
		final Shell shell = canvas.getShell();
		
		canvas.setSize(600, 600);
		
		final Image image = Activator.getImageDescriptor("icons/surface2.gif").createImage();
	
	
	
		
		GridLayout canvasGridLayout = new GridLayout();
		canvasGridLayout.numColumns = 2;
//		canvas.setLayout(canvasGridLayout);
		
		parent.setLayout(canvasGridLayout);
		parent.setSize(800, 800);
		
		GridData gridDataCanvas = new GridData();
	//	gridDataCanvas.verticalAlignment = GridData.FILL;
	//	gridDataCanvas.horizontalAlignment = GridData.FILL;
		gridDataCanvas.grabExcessVerticalSpace = true;
		gridDataCanvas.grabExcessHorizontalSpace = true;
		gridDataCanvas.minimumWidth = 605;
		gridDataCanvas.minimumHeight = 605;
		canvas.setLayoutData(gridDataCanvas);
		

		final Device device = parent.getDisplay();
		Color black = new Color (device, 0, 0, 0);
		Color green = new Color (device, 127, 255, 0);
		Color red = new Color (device, 255, 0, 0);
		
		canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent event) {
				
				event.gc.drawImage(image, 0, 0);
				event.gc.setBackground(device.getSystemColor(SWT.COLOR_GRAY));
				event.gc.fillRectangle(0, 580, 600, 20);
				event.gc.setForeground(device.getSystemColor(SWT.COLOR_BLACK));
				event.gc.drawText("Landing Zone", 260, 580);
				
				Font resultantFont = new Font(device,"Arial", 7, SWT.None );
				event.gc.setFont(resultantFont);
				
				event.gc.setLineWidth(3);
				event.gc.setAntialias(SWT.ON);
				event.gc.setBackground(device.getSystemColor(SWT.COLOR_BLACK));

				if (llvMe == null) {
					// player 1
					llvMe = new LunarLanderVisual(canvas, SWT.None,true);
				}
				if (llvYou == null) {
					// player 2
					llvYou = new LunarLanderVisual(canvas, SWT.None,false);
				}

				if (llvMe != null)
					llvMe.paintMe(event.gc);
				if (llvYou != null)
					llvYou.paintMe(event.gc);

				// lunar surface
				event.gc.drawLine(0, 600, 600, 600);

				// lunar walls
				event.gc.drawLine(0, 0, 0, 600);
				
				event.gc.drawLine(600, 0, 600, 600);

			}
		});
		
		
		
		
		// --list of games
				GridLayout gamesLayout = new GridLayout();
				gamesLayout.numColumns = 1;
			
				GridData gridData = new GridData();
				gridData.verticalAlignment = GridData.FILL;
				gridData.horizontalAlignment = GridData.FILL;
				gridData.grabExcessVerticalSpace = true;
				gridData.grabExcessHorizontalSpace = true;
				gridData.verticalSpan = 2;
				
				Group gamesGroup = new Group(parent, SWT.NONE);
				gamesGroup.setLocation(605, 0);
				gamesGroup.setText("Games");
				gamesGroup.setLayoutData(gridData);
				gamesGroup.setLayout(gamesLayout);

				ArrayList<String> defList = new ArrayList<String>();
				
				//color
				gamesGroup.setBackground(black);
				gamesGroup.setForeground(red);
				
				
				
				// table
				gamesTable = new GamesTable(gamesGroup, SWT.MULTI | SWT.H_SCROLL
						| SWT.V_SCROLL, this.getViewSite(), defList);
				
				Referee.getInstance().addObserver(GameView.gamesTable);
				
				
				
				GridData buttonGridData = new GridData();
				buttonGridData.horizontalAlignment = GridData.FILL;
			//	buttonGridData.grabExcessVerticalSpace = true;
				buttonGridData.grabExcessHorizontalSpace = true;
				
				
				// BUTTON create game
				final Button createButton = new Button(gamesGroup, SWT.PUSH);
				createButton.setText("Create");
				createButton.setLayoutData(buttonGridData);
				createButton.pack();
				// add mouse listener
				createButton.addMouseListener(new MouseListener() {
					@Override
					public void mouseDoubleClick(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseDown(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseUp(MouseEvent e) {
						
						CreateGameDialog dialog = new CreateGameDialog(shell);
						dialog.create();

						if (dialog.open() == Window.OK) {
							String gameName = dialog.getGameName();

							System.out
							.println("[GameView] Creating Internal Event");
							// generate external event
							gamesTable.add(gameName);
							EventFactory.getInstance().makeInternalChangeStatus("Waiting for player to join game: "+gameName+".");
							EventFactory.getInstance().makeInternalCreateGame(gameName,time);
								createButton.setEnabled(false);
								findButton.setEnabled(false);
								joinButton.setEnabled(false);
								
							} else {
								// need pop up msg here
							}
							

					
				}});
				createButton.setEnabled(false);
				
				
				// BUTTON join game
				joinButton = new Button(gamesGroup, SWT.PUSH);
				joinButton.setText("Join");
				joinButton.setLayoutData(buttonGridData);
				joinButton.pack();
				// add mouse listener
				joinButton.addMouseListener(new MouseListener() {
					@Override
					public void mouseDoubleClick(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseDown(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseUp(MouseEvent e) {
						System.out
								.println("[GameView] Creating Internal Event");
						// generate external event
						ISelection selection = gamesTable.getSelection();
						if(selection != null && !selection.toString().equals("<empty selection>")){
							String game = selection.toString();
							game = game.substring(1, game.length()-1);
							EventFactory.getInstance().makeInternalJoinGame(game,time);
						}
					}
				});		
				
				joinButton.setEnabled(false);

				
				// BUTTON find game
				findButton = new Button(gamesGroup, SWT.PUSH);
				findButton.setText("List Games");
				findButton.setLayoutData(buttonGridData);
				findButton.pack();
				// add mouse listener
				findButton.addMouseListener(new MouseListener() {
					@Override
					public void mouseDoubleClick(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseDown(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseUp(MouseEvent e) {
						System.out
								.println("[GameView] Creating Internal Event");
						EventFactory.getInstance().makeExternalFindGames(time);
					}
				});	
				
				findButton.setEnabled(false);

				
				// BUTTON find game
				configure = new Button(gamesGroup, SWT.PUSH);
				configure.setText("Configure");
				configure.setLayoutData(buttonGridData);
				configure.pack();
				// add mouse listener
				configure.addMouseListener(new MouseListener() {
					@Override
					public void mouseDoubleClick(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseDown(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseUp(MouseEvent e) {
						ConfigureGameServerDialog dialog = new 
								ConfigureGameServerDialog(shell,GameStarter.getInstance().getIPofGameServer());
					
						dialog.create();

						if (dialog.open() == Window.OK) {
							String serverIP = dialog.getGameServerIP();
							GameStarter.getInstance().setIPofGameServer(serverIP);
							
							Job job = new Job("GameStarter Find Job") {
						     protected IStatus run(IProgressMonitor monitor) {
						    	 GameStarter.getInstance().run();
									
						           return Status.OK_STATUS;
						        }
						     };
						  job.setPriority(Job.SHORT);
						  job.schedule(); // start as soon as possible
							
						  
						  createButton.setEnabled(true);
						  findButton.setEnabled(true);
						  joinButton.setEnabled(true);
						  
						}
					}
				});	
				
				gamesGroup.pack();

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// group
		GridData dashGridData = new GridData();
		dashGridData.horizontalAlignment = GridData.FILL;
	//	buttonGridData.grabExcessVerticalSpace = true;
		dashGridData.grabExcessHorizontalSpace = true;
		dashGridData.horizontalSpan = 2;
		
		Group group = new Group(parent, SWT.NONE);
	//	group.setLocation(0, 610);
		group.setText("Dashboard");
		// grid layout
		GridLayout groupGridLayout = new GridLayout();
		groupGridLayout.numColumns = 1;
		group.setLayout(groupGridLayout);
		group.setLayoutData(dashGridData);
		// grid layout
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		// composite
		Composite comp = new Composite(group, SWT.None);
		comp.setLayout(gridLayout);
		comp.setLayoutData(dashGridData);
		
		//color
		group.setBackground(black);
		group.setForeground(red);
		comp.setBackground(black);

		
		// LABEL gravity
		ObserverLabel gravityLabel = new ObserverLabel(comp, SWT.NONE,"down velocity (m/min)"
				,GameModel.gravityForce+"",ObserverLabel.modelType.gravity);
		gravityLabel.setLocation(0, 610);
		gravityLabel.pack();
		
		
		// LABEL fuel
		fuelLabel = new ObserverLabel(comp, SWT.NONE, "fuel", "XXX/XXX",
				ObserverLabel.modelType.fuel);
	//	fuelLabel.setLocation(0, 610);
		fuelLabel.pack();

		// LABEL rocket velocity
		speedLabel = new ObserverLabel(comp, SWT.NONE, "rocket speed(m/min)", "XXX",
				ObserverLabel.modelType.speed);
	//	speedLabel.setLocation(0, 630);
		speedLabel.pack();
		
		// LABEL actualSpeed
		actualSpeedLabel = new ObserverLabel(comp, SWT.NONE, "actual speed (m/min)", "XXX",
				ObserverLabel.modelType.actualSpeed);
	//	actualSpeedLabel.setLocation(0, 630);
		actualSpeedLabel.pack();
		
		
		// LABEL actualSpeed
		altitudeLabel = new ObserverLabel(comp, SWT.NONE, "altitude (meters)", "XXX",
				ObserverLabel.modelType.altitude);
	//	altitudeLabel.setLocation(0, 630);
		altitudeLabel.pack();


		// LABEL rotation
		rotationLabel = new ObserverLabel(comp, SWT.NONE, "trust angle", "XXX",
				ObserverLabel.modelType.rotation);
	//	rotationLabel.setLocation(0, 650);
		rotationLabel.pack();

		// new bug
		newBugLabel = new ObserverLabel(comp, SWT.NONE, "bugs/Game", "0",
				ObserverLabel.modelType.newBug);
	//	newBugLabel.setLocation(0, 650);
		newBugLabel.pack();
		
		timeLabel = new ObserverLabel(comp, SWT.NONE, "time (msE1)", "0000000000",
				ObserverLabel.modelType.time);
	//	timeLabel.setLocation(0, 650);
		timeLabel.pack();
		
		Timer.getInstance().addObserver(timeLabel);

		GridData statusData = new GridData();
		statusData.horizontalAlignment = GridData.CENTER;
		statusData.grabExcessVerticalSpace = true;
		statusData.grabExcessHorizontalSpace = true;
		statusData.minimumWidth = 400;
		statusLabel = new ObserverLabel(comp, SWT.NONE, "status", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
				ObserverLabel.modelType.status);	
		Referee.getInstance().addObserver(statusLabel);
		statusLabel.setLayoutData(statusData);
		statusLabel.pack();
		
		// button grid layout
				GridLayout buttonGridLayout = new GridLayout();
				buttonGridLayout.numColumns = 5;
				
		Composite buttonComposite = new Composite(group, SWT.None);
		buttonComposite.setLayout(buttonGridLayout);
		buttonComposite.setLayoutData(dashGridData);

		// BUTTON end turn
		Button endTurn = new Button(buttonComposite, SWT.PUSH);
		endTurn.setText("End Turn");
		endTurn.setLayoutData(dashGridData);
		endTurn.pack();
		
		endTurn.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseUp(MouseEvent e) {
						System.out
								.println("[GameView] Creating Internal Event");
						// generate internal event
						EventFactory.getInstance().makeInternalEndTurn(time);
					
			}
		});

		// BUTTON speed direction
		Button speedDirection = new Button(buttonComposite, SWT.PUSH);
		speedDirection.setText("Speed And Direction");
		speedDirection.setLayoutData(dashGridData);
		speedDirection.pack();
		// add mouse listener
		speedDirection.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseUp(MouseEvent e) {
				SpeedAngleDialog dialog = new SpeedAngleDialog(shell, 
						Referee.getInstance().getGameModel().getMe().getLunarLander().getSpeed()+"",
						0+"" );
				dialog.create();

				if (dialog.open() == Window.OK) {
					String speedStr = dialog.getSpeed();
					String rotationStr = dialog.getRotation();

					double speed = Double.parseDouble(speedStr);
					double rotation = Double.parseDouble(rotationStr);
					// checking if has values and are in correct range
					if (speed >= 0) {

						System.out
								.println("[GameView] Creating Internal Event");
						// generate internal event
						EventFactory.getInstance()
								.makeInternalSpeedRotationChange(speed,
										rotation, time);
					} else {
						// need pop up msg here
					}
				}
			}
		});


		// BUTTON fire laser
		Button fireLaser = new Button(buttonComposite, SWT.PUSH);
		fireLaser.setText("Fire Laser");
		fireLaser.setLayoutData(dashGridData);
		fireLaser.pack();
		fireLaser.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseUp(MouseEvent e) {
					
						System.out.println("[GameView] Creating Internal Event");
						// generate internal event
						EventFactory.getInstance().makeInternalFireLaser(time);
					} 
		});

		// BUTTON speed direction
//		Button startGame = new Button(buttonComposite, SWT.PUSH);
//		startGame.setText("Start Game");
//		startGame.setLayoutData(dashGridData);
//		startGame.pack();
//		// add mouse listener
//		startGame.addMouseListener(new MouseListener() {
//			@Override
//			public void mouseDoubleClick(MouseEvent e) {
//				// TODO Auto-generated method stub
//			}
//
//			@Override
//			public void mouseDown(MouseEvent e) {
//				// TODO Auto-generated method stub
//			}
//
//			@Override
//			public void mouseUp(MouseEvent e) {
//				System.out
//						.println("[TESTING - GameView] Creating External Event");
//				// generate external event
//				EventFactory.getInstance().makeExternalStartGame("000.000.000",
//						"test game", 1);
//			}
//		});
		
		// BUTTON move direction
		Button moveButton = new Button(buttonComposite, SWT.PUSH);
		moveButton.setText("Move");
		moveButton.setLayoutData(dashGridData);
		moveButton.pack();
		// add mouse listener
		moveButton.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseUp(MouseEvent e) {
				System.out
						.println("[GameView] Creating Internal Event");
				// generate external event
				EventFactory.getInstance().makeInternalMove(time);
			}
		});
		
		
		


		
		Composite empty = new Composite(parent, SWT.None);
		empty.setBackground(device.getSystemColor(SWT.COLOR_DARK_YELLOW));
		GridData dashGridData2 = new GridData();
		dashGridData2.horizontalAlignment = GridData.FILL;
		dashGridData2.horizontalAlignment = GridData.FILL;
		dashGridData2.grabExcessVerticalSpace = true;
		dashGridData2.grabExcessHorizontalSpace = true;
		dashGridData2.horizontalSpan = 2;
		dashGridData2.minimumHeight = 100;
		dashGridData2.minimumWidth = 600;
		empty.setLayoutData(dashGridData2);
		

		

		
		
		// group pack
		group.pack();
		canvas.pack();
		parent.pack();
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		gamesTable.getControl().setFocus();
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof client.Timer) {
			time = (int) arg;
		}

	}
}