package gui;

import imageview.Activator;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnLayoutData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PlatformUI;

import client.Referee;

public class GamesTable extends TableViewer implements Observer{

	List<String> games;
	ViewLabelProvider labelProvider = new ViewLabelProvider();
	ViewContentProvider contentProvider = new ViewContentProvider();
	
	public GamesTable(Composite parent, int style, IViewSite viewSite, List<String> games) {
		super(parent, style);

		this.games = games;
		
		this.setContentProvider(contentProvider);
		this.setLabelProvider(labelProvider);
		this.setSorter(new NameSorter());
		this.setInput(viewSite);
		
		Table table = this.getTable();

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.grabExcessHorizontalSpace = true;
		
		table.setLayoutData(gridData);
		Device device = Display.getCurrent ();
		Color black = new Color (device, 0, 0, 0);		
		
		Color green = new Color (device, 127, 255, 0);
		
		table.setBackground(black);
		table.setForeground(green);
	
		
	}

	
	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */

	class ViewContentProvider implements IStructuredContentProvider{
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			
			return games.toArray(new String[0]);
		}
	}

	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}

		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}

		public Image getImage(Object obj) {
			Image image = Activator.getImageDescriptor("icons/smile.png")
				.createImage();
			return image;
		}
	}

	class NameSorter extends ViewerSorter {
	}
	
	@Override
	public void update(Observable o, Object arg) {
		this.games = Referee.getInstance().getGames();
		
		Display display = PlatformUI.getWorkbench().getDisplay();
		display.asyncExec((new Runnable() {
            public void run() {
        		System.out.println("Refreshing table");
        		refresh(true, true);
            }

		}
	));
		
	}

}
