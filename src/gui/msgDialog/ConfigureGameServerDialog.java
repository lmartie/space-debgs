package gui.msgDialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ConfigureGameServerDialog extends Dialog {
	
	private String gameIP = null;
	private Text gameIPTXT;
	

	public ConfigureGameServerDialog(Shell parentShell, String string) {
		super(parentShell);
		gameIP = string;
		
	}
	
	  public void create() {
		    super.create();

	 }
	  
	  protected Control createDialogArea(Composite parent) {
		    GridLayout layout = new GridLayout();
		    layout.numColumns = 2;
		    parent.setLayout(layout);
		    
		    Label speedLabel = new Label(parent,SWT.None);
		    speedLabel.setText("Game Server IP: ");
		    gameIPTXT = new Text(parent, SWT.BORDER | SWT.MULTI);
		    gameIPTXT.setSize(100, 50);
		    gameIPTXT.setText(gameIP);
		    GridData gd = new GridData();
		    gd.minimumWidth = 200;
		    gd.verticalAlignment = GridData.FILL;
		    gd.grabExcessVerticalSpace = true;
		    gd.grabExcessHorizontalSpace = true;
		    
		    gameIPTXT.setLayoutData(gd);
		   

			return parent;
	  }
	  
	  protected void createButtonsForButtonBar(Composite compsite){
			GridLayout groupGridLayout = new GridLayout();
			groupGridLayout.numColumns = 2;
			compsite.setLayout(groupGridLayout);		  
		  
		  createOKButton(compsite);
		  createCancelButton(compsite);
	  }
	  
	  protected Button createOKButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    button.setText("okay");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  setGameServerIP(gameIPTXT.getText());
		    	
		    	  ConfigureGameServerDialog.this.okPressed();
		    	  ConfigureGameServerDialog.this.close();
		      }
		    });
		    return button;
		  }
	  
	  protected Button createCancelButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    parent.getShell().setDefaultButton(button);
		    button.setText("cancel");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  setReturnCode(CANCEL);
		    	  ConfigureGameServerDialog.this.cancelPressed();
		    	  ConfigureGameServerDialog.this.close();
		      }
		    });
		    return button;
		  }

	public String getGameServerIP() {
		return gameIP;
	}

	public void setGameServerIP(String gameIP) {
		this.gameIP = gameIP;
	}
}
