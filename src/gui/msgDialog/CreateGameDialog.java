package gui.msgDialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CreateGameDialog extends Dialog {
	
	private String gameName = null;
	private Text gameTxt;
	

	public CreateGameDialog(Shell parentShell) {
		super(parentShell);
		
	}
	
	  public void create() {
		    super.create();

	 }
	  
	  protected Control createDialogArea(Composite parent) {
		    GridLayout layout = new GridLayout();
		    layout.numColumns = 2;
		    parent.setLayout(layout);
		    
		    Label speedLabel = new Label(parent,SWT.None);
		    speedLabel.setText("Game Name: ");
		    gameTxt = new Text(parent, SWT.BORDER | SWT.MULTI);
		    gameTxt.setSize(100, 50);
		  
		    GridData gd = new GridData();
		    gd.minimumWidth = 200;
		    gd.verticalAlignment = GridData.FILL;
		    gd.grabExcessVerticalSpace = true;
		    gd.grabExcessHorizontalSpace = true;
		    
		    gameTxt.setLayoutData(gd);
		   

			return parent;
	  }
	  
	  protected void createButtonsForButtonBar(Composite compsite){
			GridLayout groupGridLayout = new GridLayout();
			groupGridLayout.numColumns = 2;
			compsite.setLayout(groupGridLayout);		  
		  
		  createOKButton(compsite);
		  createCancelButton(compsite);
	  }
	  
	  protected Button createOKButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    button.setText("okay");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  setGameName(gameTxt.getText());
		    	
		    	  CreateGameDialog.this.okPressed();
		    	  CreateGameDialog.this.close();
		      }
		    });
		    return button;
		  }
	  
	  protected Button createCancelButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    parent.getShell().setDefaultButton(button);
		    button.setText("cancel");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  setReturnCode(CANCEL);
		    	  CreateGameDialog.this.cancelPressed();
		    	  CreateGameDialog.this.close();
		      }
		    });
		    return button;
		  }

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}


	  
	
}
