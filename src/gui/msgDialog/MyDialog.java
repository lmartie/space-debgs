package gui.msgDialog;


import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class MyDialog extends Dialog {
	
	private String speed = "";
	private String direction = "";
	private Text speedTxt;
	private Text directionTxt;
	

	public MyDialog(Shell parentShell) {
		super(parentShell);

	}
	
	  public void create() {
		    super.create();

	 }
	  
	  protected Control createDialogArea(Composite parent) {
		    GridLayout layout = new GridLayout();
		    layout.numColumns = 2;
		    parent.setLayout(layout);
		    
		    Label speedLabel = new Label(parent,SWT.None);
		    speedLabel.setText("speed (MPH): ");
		    speedTxt = new Text(parent, SWT.BORDER | SWT.MULTI);
		    speedTxt.setSize(100, 50);
		    
		    Label directionLabel = new Label(parent,SWT.None);
		    directionLabel.setText("direction (Rotation Angle): ");
		    directionTxt = new Text(parent, SWT.BORDER | SWT.MULTI);
		    directionTxt.setSize(100, 50);
		    
		   
		    
		    speedTxt.pack();
		    directionTxt.pack();
		    speedLabel.pack();
		    directionLabel.pack();
		    
		   
			return parent;
	  }
	  
	  protected void createButtonsForButtonBar(Composite compsite){
			GridLayout groupGridLayout = new GridLayout();
			groupGridLayout.numColumns = 2;
			compsite.setLayout(groupGridLayout);		  
		  
		  createOKButton(compsite);
		  createCancelButton(compsite);
	  }
	  
	  protected Button createOKButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    button.setText("okay");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  speed = speedTxt.getText();
		    	  direction = directionTxt.getText();
		    	  MyDialog.this.okPressed();
		    	  MyDialog.this.close();
		      }
		    });
		    return button;
		  }
	  
	  protected Button createCancelButton(Composite parent) {
		    Button button = new Button(parent, SWT.PUSH);
		    parent.getShell().setDefaultButton(button);
		    button.setText("cancel");
		    button.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent event) {
		    	  setReturnCode(CANCEL);
		    	  MyDialog.this.cancelPressed();
		    	  MyDialog.this.close();
		      }
		    });
		    return button;
		  }

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	  
	  
	  
}
