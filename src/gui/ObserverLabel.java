package gui;

import imageview.views.GameView;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;

import client.GameModel;
import client.LunarLanderModel;
import client.Referee;

public class ObserverLabel extends Composite implements Observer{
	LunarLanderModel model;
	String title;
	Label titleLabel = new Label(this, SWT.None);
	Label modelLabel = new Label(this, SWT.None);
	public enum modelType {speed, fuel, rotation, newBug, time, actualSpeed, altitude, gravity, status};
	private modelType type;
	
	public ObserverLabel(Composite parent, int style, String title,
			String defaultValue, ObserverLabel.modelType modelType) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		GridLayout groupGridLayout = new GridLayout();
		groupGridLayout.numColumns = 2;
		this.setLayout(groupGridLayout);
		
		Device device = Display.getCurrent ();
		Color black = new Color (device, 0, 0, 0);
		this.setBackground(black);
		
		
		Color green = new Color (device, 127, 255, 0);
		this.setForeground(green);
		
		Color gray = new Color (device, 211, 211, 211);
		this.setForeground(gray);
		
		modelLabel.setForeground(green);
		titleLabel.setForeground(gray);
		
		titleLabel.setText(title);
		modelLabel.setText(defaultValue);
		type = modelType;
	}


	
	
	@Override
	public void update(final Observable o, final Object arg) {
		

		Display display = PlatformUI.getWorkbench().getDisplay();
		display.asyncExec((new Runnable() {
            public void run() {
            	
        		
        		if(type == modelType.speed){
        			model = (LunarLanderModel) o;
        			double speed = model.getSpeed();
        			modelLabel.setText(speed+"");
        		}else if(type == modelType.rotation){
        			model = (LunarLanderModel) o;
        			double rotation = model.getRotation();
        			modelLabel.setText(rotation+"");
        		}else if(type == modelType.fuel){
        			model = (LunarLanderModel) o;
        			double fuel = model.getFuel();
        			double precentage = fuel;
        			modelLabel.setText(precentage+"%");
        		}else if(type == modelType.newBug){
        			model = (LunarLanderModel) o;
        			//int fuel = model.getFuel();
        			modelLabel.setText(model.getBugs()+"");
        		}else if(type == modelType.time){
        			int time = (Integer)arg;
        			modelLabel.setText(time+"");
        		}else if(type == modelType.actualSpeed){
        			model = (LunarLanderModel) o;
        			modelLabel.setText(Math.round(model.getActualSpeed())+"");
        		}else if(type == modelType.altitude){
        			model = (LunarLanderModel) o;
        			modelLabel.setText(Math.round(model.getAltitude())+"");
        		}else if(type == modelType.gravity){
        			modelLabel.setText(GameModel.gravityForce+"");
        		}else if(type == modelType.status){
        			modelLabel.setText(Referee.getInstance().getStatus());
        		}
            
            	
        		getParent().redraw();
        		getParent().update();
            }
        }));
		
	}

}
