package gui;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import client.LunarLanderModel;

import shapes.Line;
import shapes.Triangle;

public class LunarLanderVisual extends Composite implements Observer {

	private LunarLanderModel model;
	private boolean me;

	public LunarLanderVisual(Composite parent, int style, boolean me) {
		super(parent, style);
		this.me = me;
	}

	public void paintMe(GC gc) {

		if (model != null) {
			Triangle body = model.getBody();
			Device device = gc.getDevice();
			gc.setBackground(device.getSystemColor(SWT.COLOR_YELLOW));
			// body
						gc.fillPolygon(new int[] { body.getOne().asSWTPoint().x, body.getOne().asSWTPoint().y,
								body.getTwo().asSWTPoint().x, body.getTwo().asSWTPoint().y, body.getThree().asSWTPoint().x,
								body.getThree().asSWTPoint().y });
						
						Color orange = new Color (device, 255, 153, 51);
						gc.setForeground(orange);
						gc.drawLine(body.getBase().getOne().asSWTPoint().x, body.getBase().getOne().asSWTPoint().y,
								body.getBase().getTwo().asSWTPoint().x, body.getBase().getTwo().asSWTPoint().y);
			
						gc.setBackground(device.getSystemColor(SWT.COLOR_BLACK));
			//direction of thrust
			gc.setForeground(device.getSystemColor(SWT.COLOR_RED));
			gc.drawLine(model.getThrustDirection().getOne().asSWTPoint().x, model.getThrustDirection().getOne().asSWTPoint().y,
					model.getThrustDirection().getTwo().asSWTPoint().x, model.getThrustDirection().getTwo().asSWTPoint().y);
			gc.drawOval((model.getThrustDirection().getOne().asSWTPoint().x-2), model.getThrustDirection().getOne().asSWTPoint().y, 3, 3);
			gc.setForeground(device.getSystemColor(SWT.COLOR_BLACK));
			
			
			//resultant
			
			gc.setLineStyle(SWT.LINE_DASH);
			gc.setForeground(device.getSystemColor(SWT.COLOR_GREEN));
			gc.drawLine(model.getResultant().getOne().asSWTPoint().x, model.getResultant().getOne().asSWTPoint().y,
					model.getResultant().getTwo().asSWTPoint().x, model.getResultant().getTwo().asSWTPoint().y);
			gc.setLineStyle(SWT.LINE_SOLID);
			
			gc.drawOval(model.getResultant().getTwo().asSWTPoint().x-2, model.getResultant().getTwo().asSWTPoint().y, 3, 3);
			gc.setForeground(device.getSystemColor(SWT.COLOR_BLACK));
			
		
			
			
			//resultant

			
			gc.setBackground(device.getSystemColor(SWT.COLOR_BLACK));
			gc.setForeground(device.getSystemColor(SWT.COLOR_WHITE));
			
			Font resultantFont = new Font(device,"Arial", 7, SWT.None );
			gc.setFont(resultantFont);
			
			if(me)
			gc.drawText("me",body.getCenter().asSWTPoint().x-4,
					body.getCenter().asSWTPoint().y-4);
//			gc.drawText(Math.round(model.getActualSpeed())+"", 	body.getCenter().asSWTPoint().x-4,
//					body.getCenter().asSWTPoint().y-4);
			
			gc.setBackground(device.getSystemColor(SWT.COLOR_BLACK));
			gc.setForeground(device.getSystemColor(SWT.COLOR_BLACK));
			
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		model = (LunarLanderModel) o;
		
		Display display = PlatformUI.getWorkbench().getDisplay();
		display.asyncExec((new Runnable() {
            public void run() {
        		getParent().redraw();
        		getParent().update();
            }
        }));

	}
}
