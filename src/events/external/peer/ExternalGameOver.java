package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import client.LunarLanderModel;

public class ExternalGameOver extends PeerEvent{

	public enum OpponentStatus {win,lose, tie};
	
	private OpponentStatus opponentStatus;
	private static String lose = "LOSE";
	private static String win = "WIN";
	private static String tie = "TIE";
	
	public ExternalGameOver(OpponentStatus opponentStatus){
		this.setOpponentStatus(opponentStatus);
	}
	
	@Override
	public Map toHashMap() {
		Map m = new HashMap();
		m.put("Type", "ExternalGameOver");
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		m.put("opponentStatus", this.getOpponentStatusAsString());
		return m;
	}

	public OpponentStatus getOpponentStatus() {
		return opponentStatus;
	}

	public void setOpponentStatus(OpponentStatus opponentStatus) {
		this.opponentStatus = opponentStatus;
	}
	
	/**
	 * 
	 * @return the actual hit module as a string
	 */
	public String getOpponentStatusAsString(){
		if(opponentStatus == OpponentStatus.win){
			return this.win;
		}else if(opponentStatus == OpponentStatus.lose){
			return this.lose;
		}else if(opponentStatus == OpponentStatus.tie){
			return this.tie;
		}
		return null;
	}
	
	/**
	 * 
	 * @param str
	 * @return converts a string to a module
	 */
	public static OpponentStatus stringToOpponentStatus(String str){
		if(str.equals(tie)){
			return OpponentStatus.tie;
		}else if(str.equals(lose)){
			return OpponentStatus.lose;
		}else if(str.equals(lose)){
			return OpponentStatus.win;
		}
		return null;
	}

}
