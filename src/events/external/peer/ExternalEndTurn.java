package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import events.external.ExternalEvent;

public class ExternalEndTurn extends PeerEvent{
	

	@Override
	public Map toHashMap() {
		// TODO: This event class is empty.
		Map m = new HashMap();
		m.put("Type", "ExternalEndTurn");
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		return m;
	}

}
