package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.dom.ThisExpression;

import events.external.ExternalEvent;

public class CoordinatesChange extends PeerEvent{

	public int x;
	public int y;
	
	public CoordinatesChange(int x, int y){
		this.x = x;
		this.y = y;
	}

	@Override
	public Map toHashMap() {
		Map m = new HashMap();
		m.put("Type", "CoordinatesChange");
		m.put("x", Integer.toString(x));
		m.put("y", Integer.toString(y));
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		return m;
	}
}
