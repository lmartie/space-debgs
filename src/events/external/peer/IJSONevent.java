package events.external.peer;

import org.json.simple.JSONObject;

public interface IJSONevent {

	public JSONObject toJSON();
}
