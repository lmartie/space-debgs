package events.external.peer;

import java.util.Map;


public interface IMapEvent {

	@SuppressWarnings("rawtypes")
	public Map toHashMap();
}
