package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import events.external.ExternalEvent;

public class RotationChange extends PeerEvent{

	private double rotation;
	
	public RotationChange(double rotation){
		this.setRotation(rotation);
	}
	
	public Map toHashMap(){
		Map m = new HashMap();
		m.put("Type", "RotationChange");
		m.put("rotation", Double.toString(rotation));
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		return m;	
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

}
