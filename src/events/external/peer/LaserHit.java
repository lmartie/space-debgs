package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import client.LunarLanderModel;
import events.external.ExternalEvent;

public class LaserHit extends PeerEvent{

	private LunarLanderModel.Modules hitModule;
	private static String speed = "SPEED";
	private static String rotation = "ROTATION";
	private static String rocket = "ROCKET";
	private static String fuel = "FUEL";
	
	public LaserHit(LunarLanderModel.Modules hitModule){
		this.setHitModule(hitModule);
	}

	@Override
	public Map toHashMap() {
		Map m = new HashMap();
		m.put("Type", "LaserHit");
		m.put("Modules", this.getHitModuleAsString());	
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		return m;
	}

	public LunarLanderModel.Modules getHitModule() {
		return hitModule;
	}

	public void setHitModule(LunarLanderModel.Modules hitModule) {
		this.hitModule = hitModule;
	}
	
	/**
	 * 
	 * @return the actual hit module as a string
	 */
	public String getHitModuleAsString(){
		if(hitModule == LunarLanderModel.Modules.speed){
			return this.speed;
		}else if(hitModule == LunarLanderModel.Modules.fuel){
			return this.fuel;
		}else if(hitModule == LunarLanderModel.Modules.rocket){
			return this.rocket;
		}else if(hitModule == LunarLanderModel.Modules.rotation){
			return this.rotation;
		}
		return null;
	}
	
	/**
	 * 
	 * @param str
	 * @return converts a string to a module
	 */
	public static LunarLanderModel.Modules stringToModule(String str){
		if(str.equals(speed)){
			return LunarLanderModel.Modules.speed;
		}else if(str.equals(fuel)){
			return LunarLanderModel.Modules.fuel;
		}else if(str.equals(rocket)){
			return LunarLanderModel.Modules.rocket;
		}else if(str.equals(rotation)){
			return LunarLanderModel.Modules.rotation;
		}
		return null;
	}

}
