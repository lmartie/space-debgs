package events.external.peer;

import java.util.HashMap;
import java.util.Map;

import events.external.ExternalEvent;

public class ExternalSpeedRotationChange extends PeerEvent{

	private double rotation;
	private double speed;
	
	public ExternalSpeedRotationChange(double rotation, double speed){
		this.setRotation(rotation);
		this.setSpeed(speed);
	}
	
	public Map toHashMap(){
		Map m = new HashMap();
		m.put("Type", "ExternalSpeedRotationChange");
		m.put("rotation", Double.toString(rotation));
		m.put("speed", Double.toString(speed));
		m.put("timeStamp", Integer.toString(this.getTimeStamp()));
		return m;	
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

}
