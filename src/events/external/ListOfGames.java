package events.external;

import java.util.List;

public class ListOfGames extends ExternalEvent{

	private List<String> games;
	
	public ListOfGames(List<String> games) {
		this.setGames(games);
	}

	public List<String> getGames() {
		return games;
	}

	public void setGames(List<String> games) {
		this.games = games;
	}

}
