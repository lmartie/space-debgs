package events.external;

public class StartGame extends ExternalEvent{

	
	private int playerNumber;
	
	public StartGame(int playerNumber){
		this.setPlayerNumber(playerNumber);
	}

	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}
}
