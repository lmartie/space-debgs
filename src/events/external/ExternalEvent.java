package events.external;

import events.Event;


public class ExternalEvent extends Event {


	public String toString(){
		String string = "IP: "+this.getIP()+", Game Name: "+this.getGameName()+",Type: "+
					this.getClass()+", time: "+this.getTimeStamp();
		
		return string;
	}
}
