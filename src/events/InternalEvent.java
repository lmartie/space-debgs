package events;


public class InternalEvent extends Event {

	
	/**
	 * 
	 * SpeedDrectionChange should have speed first, direction second in the values.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @author lee
	 *
	 */
	public enum type {SpeedDrectionChange, FireLaser, DeBug, EndTurn};
	
	public type eventType = null;
	
	public InternalEvent(type eventType){
		this.eventType  = eventType;
	}
	
	public String toString(){
		String string = "IP: "+this.IP+", Game Name: "+this.gameName+",Type: "+
					this.eventType+", value: "+this.value+", time: "+this.timeStamp;
		
		return string;
	}
	
	
}
