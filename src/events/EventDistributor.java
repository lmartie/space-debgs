package events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class EventDistributor extends Observable implements Observer{

	private static EventDistributor eventDis;
	
	private EventDistributor(){}
	
	public static EventDistributor getInstance(){
		if(eventDis == null){
			eventDis = new EventDistributor();
		}
		return eventDis;
	}
	
	public enum service {lunarModelUpdate};
	
	public HashMap <EventDistributor.service,List<Observable>> publishers
			= new HashMap <EventDistributor.service,List<Observable>>();
	
	public HashMap <EventDistributor.service,List<Observer>> subscribers
	= new HashMap <EventDistributor.service,List<Observer>>();
	
	public void addSubscriber(Observer observer, EventDistributor.service service){
		List<Observer> observers = subscribers.get(service);
		
		if(observers == null){
			observers = new ArrayList<Observer>();
			observers.add(observer);
			subscribers.put(service, observers);
		}else{
			observers.add(observer);
			subscribers.put(service, observers);
		}
	}
	
	public void addPublisher(Observable observable, EventDistributor.service service){
		List<Observable> observables = publishers.get(service);
		
		if(observables == null){
			observables = new ArrayList<Observable>();
			observables.add(observable);
			publishers.put(service, observables);
		}else{
			observables.add(observable);
			publishers.put(service, observables);
		}
		
		observable.addObserver(this);
		
	}
	
	@Override
	public void update(Observable o, Object arg) {
		EventDistributor.service service = (EventDistributor.service)arg;		
		List<Observer> subs = this.subscribers.get(service);
		for(Observer observer: subs){
			observer.update(o, arg);
		}
		
	}

}
