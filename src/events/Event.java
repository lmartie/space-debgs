package events;

import java.util.ArrayList;
import java.util.List;

public abstract class Event {
	
	private String IP = "";
	private String gameName = "";
	private int timeStamp = 0;
	
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public int getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(int timeStamp) {
		this.timeStamp = timeStamp;
	}

}
