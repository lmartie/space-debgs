package events;

import events.InternalEvent.type;


public class ExternalEvent extends Event {

	/**
	 * 
	 * 
	 * Start Game should include IP address of opponent and your player number
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @author lee
	 *
	 */
	public enum type {CoordinatesChange, StartGame, FindGame, ListOfGames, JoinGame, CreateGame, LaserHit};
	
	public type eventType = null;
	
	public ExternalEvent(type eventType){
		this.eventType  = eventType;
	}
	
	public String toString(){
		String string = "IP: "+this.IP+", Game Name: "+this.gameName+",Type: "+
					this.eventType+", value: "+this.value+", time: "+this.timeStamp;
		
		return string;
	}
}
