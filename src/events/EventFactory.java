package events;

import java.util.ArrayList;
import java.util.List;

import client.EventCollection;
import events.external.CreateGame;
import events.external.ExternalEvent;
import events.external.FindGame;
import events.external.GameServerListRefresh;
import events.external.JoinGame;
import events.external.ListOfGames;
import events.external.StartGame;
import events.internal.ChangeStatus;
import events.internal.EndTurn;
import events.internal.FireLaser;
import events.internal.InternalEvent;
import events.internal.InternalLLError;
import events.internal.MoveLander;
import events.internal.SpeedRotationChange;

public class EventFactory {
	
	public static EventFactory eventFactory;
	
	private EventFactory(){}
	
	
	public static EventFactory getInstance(){
		if(eventFactory == null){
			eventFactory = new EventFactory();
		}
		
		return eventFactory;
	}
	
	public void makeInternalListGames(List<String> games, int time){
		if(games == null){
			games = new ArrayList<String>();
		}
		ListOfGames listOfGames = new ListOfGames(games);
		listOfGames.setTimeStamp(time);
		EventCollection.getInstance().addEvent(listOfGames);
	}
	
	public void makeInternalMove(int time){
		MoveLander ml = new MoveLander();
		ml.setTimeStamp(time);
		System.out.println("[EventFactory] Adding Internal Event to Events Collection");
		EventCollection.getInstance().addEvent(ml);
	}
	
	public void makeInternalError(String errorMsg){
		InternalLLError inE = new InternalLLError();
		inE.setErrorMsg(errorMsg);
		inE.setTimeStamp(0);
		System.out.println("[EventFactory] Adding Internal Event to Events Collection");
		EventCollection.getInstance().addEvent(inE);
	}
	
	
	public void makeInternalFireLaser(int time){
		FireLaser fireLaser = new FireLaser();
		fireLaser.setTimeStamp(time);
		System.out.println("[EventFactory] Adding Internal Event to Events Collection");
		EventCollection.getInstance().addEvent(fireLaser);
	}
	public void makeInternalSpeedRotationChange(double speed, double rotation, int time){
		SpeedRotationChange srChange = new SpeedRotationChange(speed, rotation);
		srChange.setTimeStamp(time);
		System.out.println("[EventFactory] Adding Internal Event to Events Collection");
		EventCollection.getInstance().addEvent(srChange);
	}
	
	public void makeInternalChangeStatus(String status){
		ChangeStatus changeStatus = new ChangeStatus(status);
		changeStatus.setTimeStamp(0);
		System.out.println("[EventFactory] Adding Internal Event to Events Collection");
		EventCollection.getInstance().addEvent(changeStatus);
	}
	

	public void makeExternalStartGame(String IP, String gameName, int playerNumber){
		StartGame startGame = new StartGame(playerNumber);
		startGame.setGameName(gameName);
		startGame.setIP(IP);
		System.out.println("[EventFactory] Adding External Event to Events Collection");
		EventCollection.getInstance().addEvent(startGame);
	}


	public void makeInternalCreateGame(String gameName, int time) {
		CreateGame createGame = new CreateGame();
		createGame.setGameName(gameName);
		createGame.setTimeStamp(time);
		System.out.println("[EventFactory] Adding External Event to Events Collection");
		EventCollection.getInstance().addEvent(createGame);
	}
	
	public void makeExternalGameServerListRefresh(int time){
		GameServerListRefresh refresh = new GameServerListRefresh();
		refresh.setTimeStamp(time);
		System.out.println("[EventFactory] Adding External Event to Events Collection");
		EventCollection.getInstance().addEvent(refresh);
	}


	public void makeInternalJoinGame(String game, int time) {
		System.out.println("[EventFactory] Adding External Event to Events Collection");
		JoinGame joinGame = new JoinGame();
		joinGame.setGameName(game);
		joinGame.setTimeStamp(time);
		EventCollection.getInstance().addEvent(joinGame);
	}


	public void makeExternalFindGames(int time) {
		FindGame findGame = new FindGame();
		findGame.setTimeStamp(time);
		System.out.println("[EventFactory] Adding External Event to Events Collection");
		EventCollection.getInstance().addEvent(findGame);
	}


	public void makeInternalEndTurn(int time) {
		EndTurn endTurn = new EndTurn();
		endTurn.setTimeStamp(time);
		EventCollection.getInstance().addEvent(endTurn);
		
	}


}
