package events.internal;

public class InternalLLError extends InternalEvent{
	
	private String errorMsg;
	
	public void InternalError(String msg){
		this.setErrorMsg(msg);
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
