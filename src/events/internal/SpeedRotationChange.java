package events.internal;

public class SpeedRotationChange extends InternalEvent{

	private double speed;
	private double rotation;
	
	public SpeedRotationChange(double speed, double rotation){
		this.setSpeed(speed);
		this.setRotation(rotation);
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
}
