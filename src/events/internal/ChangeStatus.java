package events.internal;

public class ChangeStatus extends InternalEvent{

	private String status;
	
	public ChangeStatus(String status){
		this.setStatus(status);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
