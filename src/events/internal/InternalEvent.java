package events.internal;

import events.Event;


public class InternalEvent extends Event {

	
	public String toString(){
		String string = "IP: "+this.getIP()+", Game Name: "+this.getGameName()+",Type: "+
					this.getClass()+", time: "+this.getTimeStamp();
		
		return string;
	}
	
	
}
