package bugs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.tools.JavaCompiler;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IOpenable;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jdt.ui.*;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.ui.ide.IDE;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.ui.part.*;
/**
 * referenced: https://sdqweb.ipd.kit.edu/wiki/JDT_Tutorial:
 * _Creating_Eclipse_Java_Projects_Programmatically
 * 
 * @author lee
 * 
 */
public class BugGenerator {

	private static BugGenerator generator = null;

	private BugGenerator() {
	}

	public static BugGenerator getInstance() {
		if (generator == null) {
			generator = new BugGenerator();
		}
		return generator;
	}

	/**
	 * Replaces LunarLanderModel.java with name of file under bugs directory
	 * @param name
	 * @throws CoreException
	 */
	public void generateBug(final String name) throws CoreException {
		Display display = PlatformUI.getWorkbench().getDisplay();
		display.asyncExec((new Runnable() {
			public void run() {

				String projectName = "SpaceDebugs";

				IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				IProject project = root.getProject(projectName);
				try {
					if (!project.exists()) 
						project.create(null); 
						if (!project.isOpen()) 
						project.open(null); 

					IProjectDescription description = project.getDescription();
					description
							.setNatureIds(new String[] { JavaCore.NATURE_ID });
					project.setDescription(description, null);

					IJavaProject javaProject = JavaCore.create(project);

					
					IPath path = new Path("src/client/LunarLanderModel.java");				
					
					File bugFile = new File("lunarLander/SpaceDebugs/bugs/"+name);

					final IFile file = project.getFile(path);
					IWorkbench bench = PlatformUI.getWorkbench();

					IWorkbenchPage page = null;

					IWorkbenchWindow window = bench.getActiveWorkbenchWindow();
					if (window != null) {
						page = window.getActivePage();
					}
					if (page == null) {
						IWorkbenchWindow[] windows = PlatformUI.getWorkbench()
								.getWorkbenchWindows();
						for (int i = 0; i < windows.length; i++) {
							if (windows != null) {

								window = windows[i];
								page = window.getActivePage();
								if (page != null)
									break;
							}

						}
					}
					
					IEditorDescriptor desc = PlatformUI.getWorkbench().
					        getEditorRegistry().getDefaultEditor(file.getName());
					
					FileEditorInput editorInput = new FileEditorInput(file);
					IEditorPart editor = page.openEditor(editorInput, desc.getId());
					
					
					ICompilationUnit unit = JavaCore.createCompilationUnitFrom(file); 
					String source = ((ICompilationUnit) unit).getBuffer()
							.getContents();

					final Document document = new Document(source);

					
					try {
						apply(document, ((ICompilationUnit) unit).getBuffer(),
								0, ((ICompilationUnit) unit), bugFile);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					page.saveEditor(editor, false);
				

				} catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}));

	}

	private void apply(IDocument document, IBuffer buffer, int insertionPoint,
			ICompilationUnit originalUnit, File bugFile)
			throws FileNotFoundException {

		try {

			buffer.replace(insertionPoint, buffer.getLength(), "");
			Scanner scan = new Scanner(bugFile);
			scan.useDelimiter("\\Z");
			String output = scan.next();

			buffer.replace(insertionPoint, 0, output);
			CompilationUnit unit = ((ICompilationUnit) originalUnit).reconcile(
					ICompilationUnit.NO_AST, false, null, null);

		} catch (JavaModelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}
