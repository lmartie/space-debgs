package server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



/**
 * 
 * @author hsiaohsy
 *
 */
public class Games {
	Map<String, Game> allGames;
	
	public Games() {
		allGames = new HashMap<String, Game>();
	}
	
	
	public synchronized void createGame(String game_name, 
								String player_ip, ClientWorker cw) 
	{
		allGames.put(game_name, new Game(game_name, player_ip, cw));
		System.out.print(game_name + " is created!!!");
	}
	
	
	public JSONArray getOpenGames() {
		JSONArray output = new JSONArray();
		
		if(allGames.size() > 0) {
			
			// Move next key and value of Map by iterator
			@SuppressWarnings("rawtypes")
			Iterator it = allGames.entrySet().iterator();
			
			while(it.hasNext()) {
				Map.Entry m = (Map.Entry<String, Game>) it.next();
				
				// Key is the name of game
				String key =  (String) m.getKey();
				Game g = (Game) m.getValue();
				System.out.println("Game is full?" + g.isFull());
				if(!g.isFull()) {
					output.add(key);
				}					
			}			
		} 
		
		return output;
	}
	
	public Game getGameByName(String name) {
		if(allGames.size() > 0) {
			

			Iterator it = allGames.entrySet().iterator();
			
			while(it.hasNext()) {
				Map.Entry m = (Map.Entry<String, Game>) it.next();
				
				// Key is the name of game
				String key =  (String) m.getKey();
				Game g = (Game) m.getValue();
				System.out.print(name + " ?= " + key +  " ?= " + g.getName());
				if(key.equals(name))
					return g;
			}			
		} 
		System.out.println(allGames.size() + "(num of games) GET GAME BY NAME");
		return (Game) allGames.get(name);
	}

}
