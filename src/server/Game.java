package server;

import java.util.ArrayList;

public class Game {
	private int id;
	private String name;
	private String[] playerIP;
	private boolean full;
	private ClientWorker cworker;
	
	public Game(String name, String ip, ClientWorker cw) {
		this.name = name;
		playerIP = new String[2];
		playerIP[0] = ip;
		playerIP[1] = "";
		cworker = cw;
		full = false;
		
	}
	
	public void setFull() {
		full = true;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isFull() {
		return full;
	}
	
	public void addPlayer(String ip) {
		playerIP[1] = ip;
	}
	
	public String getPeerIP(String requesterIP) {
				
		// TODO: Check for exception
		if(!playerIP[0].equals(requesterIP)) {
			return playerIP[0];
		} 
		
		return playerIP[1];
	}
	
	public void notifyJOIN() {
		System.out.println("NOTIFY JOIN" + cworker.join);

		cworker.join = true;
		synchronized(cworker){
			cworker.notify();
		}
		System.out.println("NOTIFY JOIN" + cworker.join);
		System.out.println(cworker);
	}
	
		
	
}
