package server;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RequestProcessor {
	private static RequestProcessor rp = null;
	
	Games allGames = null;
	
	
	private RequestProcessor() {
		
		allGames = new Games();
	}
	
	public static RequestProcessor getInstence() {
		if(rp == null) {
			rp = new RequestProcessor();
		}		
		return rp;
	}
	
	
	@SuppressWarnings("unchecked")
	public JSONObject findGames() {
		JSONObject jsonOut = new JSONObject();
	
		JSONArray games = allGames.getOpenGames();
		if(games.size() > 0) {
			jsonOut.put("Description", "LIST_GAME");
			jsonOut.put("Games", games);
		} else {
			jsonOut.put("Description", "NO_GAME");
		}
		
		return jsonOut;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject createGame(String gameName, String ip, ClientWorker cw) {
		JSONObject jsonOut = new JSONObject();
		System.out.println(allGames);
		allGames.createGame(gameName, ip, cw);
		jsonOut.put("Description", "GAME_CREATED");
		

		return jsonOut;
	}
	
	public JSONObject getGameStartJSON(String gameName, String ip) {		
		JSONObject jsonOut = new JSONObject();
		Game game = allGames.getGameByName(gameName);

		jsonOut.put("Description", "START_GAME");
		jsonOut.put("IP Address", game.getPeerIP(ip));
				
		return jsonOut;	
	}
	
	@SuppressWarnings("unchecked")
	public synchronized JSONObject joinGame(String gameName, String ip) {
		JSONObject jsonOut = new JSONObject();
		
		Game game = allGames.getGameByName(gameName);
		
		// Check and see if the game is full
		if(game.isFull()){
			jsonOut.put("Description", "GAME_FULL");
			return jsonOut;
		}
		
		game.setFull();
		game.addPlayer(ip);
		
		game.notifyJOIN();

		jsonOut.put("Description", "START_GAME");
		jsonOut.put("IP Address", game.getPeerIP(ip));
				
		return jsonOut;
	}

	
	@SuppressWarnings("rawtypes")
	public Map msgParser(String inMsg) {
		JSONParser parser = new JSONParser();
		 Map json = null;
		
		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}
			
			public Map createObjectContainer() {
				return new LinkedHashMap();
			}
		};
		
		try{
		    json = (Map)parser.parse(inMsg, containerFactory);
		    Iterator iter = json.entrySet().iterator();
		    while(iter.hasNext()){
		    	Map.Entry entry = (Map.Entry)iter.next();
		    }
		                        
		  }
		  catch(ParseException pe){
		    System.out.println(pe);
		  }
		
		  return json;
	}

}
