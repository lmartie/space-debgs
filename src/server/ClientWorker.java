package server;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

import org.json.simple.JSONObject;

import server.RequestProcessor;

class ClientWorker implements Runnable {
	private static RequestProcessor reqProc = null;
	public boolean join = false;
	private Socket client;
	private PrintWriter out = null;
	String cli_addr;
	
	// Constructor
	ClientWorker(Socket client) {
		reqProc = RequestProcessor.getInstence();
		this.client = client;
		this.cli_addr = client.getInetAddress().getHostAddress();
	}
					
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		BufferedReader in = null;
		String line;
		String gameName="";
		
		
		System.out.println("Connection received from " + cli_addr);
		
		try {
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			out = new PrintWriter(client.getOutputStream(), true);	
			
		} catch (IOException e) {
			System.out.println("in or out failed");
			System.exit(-1);
		}
		
		while(true) {
			try {
				line = in.readLine();		
				System.out.println(line + " from " + cli_addr);
				
				if(line.equals("CLOSE_CONNECTION")) {
					out.println(line);
					break;
				}
				
				Map clientReq = reqProc.msgParser(line);
				JSONObject jsonOut = new JSONObject();
				
				// Get the request description
				String reqStr = clientReq.get("Description").toString();
				
				if(reqStr.equals("FIND_GAME")) {
					
					// Get a list of game names
					jsonOut = reqProc.findGames();
					
					// Send the message to client
					sendMessage(jsonOut);
					
				} else if(reqStr.equals("CREATE_GAME")) {
					
					gameName = clientReq.get("Game Name").toString();
					
					// Get the confirmation from request processor
					jsonOut = reqProc.createGame(gameName, cli_addr, this);
					
					// Send the confirmation message to client
					sendMessage(jsonOut);
					
				} else if(reqStr.equals("JOIN_GAME")) {
					gameName = clientReq.get("Game Name").toString();
					
					// Get the confirmation from request processor
					jsonOut = reqProc.joinGame(gameName, cli_addr);
					
					// Send the confirmation message to client
					sendMessage(jsonOut);
				} else if(reqStr.equals("WAITING_FOR_PEER")) {
					System.out.println(cli_addr + 
							" is waiting on an opponent to join");
					//wait on server to set join to true (after CREATing GAME)
					synchronized(this) {
						try {
							this.wait();
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					jsonOut.clear();
					jsonOut = reqProc.getGameStartJSON(gameName, cli_addr);
					sendMessage(jsonOut);
					join = false;
				}else {
				
					System.out.print("Invalid request!");
					// Send a error msg to client
				}
				
//				respondToClient(line);

				
			} catch (IOException e) {
				System.out.println("Read failed. Force the disconnection.");			
				try {
					client.close();
					in.close();
					out.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				System.exit(-1);
			}
		}
		
	}
	
	
	
	@SuppressWarnings("unused")
	private void sendMessage(JSONObject obj)
	{
		String msg = obj.toString();
		System.out.println("Sending: " + msg);
		out.println(msg);
	}
	
			
}