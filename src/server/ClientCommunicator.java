package server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class ClientCommunicator {
	
	// The server socket.
	private static ServerSocket serverSocket = null;
	// The client socket.
	private static Socket clientSocket = null;
	
	private static int portNum = 2004;
	
	RequestProcessor reqProc = null;

	ClientCommunicator(){}
	
	void run()
	{

		/*
		 * Open a server socket on the portNum.
		 */
		try {
			serverSocket = new ServerSocket(portNum);
		} catch (IOException e) {
			System.out.println("Could not listen on port" + portNum);
		    System.exit(-1);
		}
		
		/*
		 * Create a client socket for each connection and pass it to a new client thread.
		 */
		while(true) {
			ClientWorker w;
			try{
				System.out.println("Waiting for connection");
				 
				// serverSocket.accept() returns a client connection
				w = new ClientWorker(serverSocket.accept());
				Thread t = new Thread(w);
				t.start();
				
			   
			} catch (IOException e) {
				System.out.println("Accept failed: 4444");
				e.printStackTrace();
			    System.exit(-1);
			}
		}

		
	}
	
	

	
	public static void main(String args[])
	{
		ClientCommunicator server = new ClientCommunicator();
		
		server.run();		
	}	
	
	



}