package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import events.EventFactory;
import events.external.ExternalEvent;

/**
 * Only talks to server
 * 
 * @author hsiaohsy
 *
 */
public class GameStarter implements IGameStarter{
	private static GameStarter gameStarter = null;
	
 	private String message;

	// The client socket
	private static Socket clientSocket = null;
	// The output stream
	private PrintWriter out;
	// The input stream
	private BufferedReader in;
	// The port number
	private int portNum = 2004;
	private String IPofGameServer = "";
	private GameStarter() {

	}

	public static GameStarter getInstance() {
		if (gameStarter == null) {
			gameStarter = new GameStarter();
		}
		return gameStarter;
	}



	public void run()
	{
		
		try{
			//1. creating a socket to connect to the server
			//clientSocket = new Socket("128.195.4.82", portNum);
			clientSocket = new Socket(getIPofGameServer(), portNum);
			System.out.println("Connected to localhost in port " + portNum);
			
			//2. get Input and Output streams
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		}
		catch(UnknownHostException unknownHost){
			System.err.println("You are trying to connect to an unknown host!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		
	}
	/**
	 * Will fail if array does not contain String!!!!!!
	 * 
	 * @param array
	 * @return
	 */
	public List<String> jsonArrayStringToJavaList(JSONArray array){
		int length = array.size();
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i<length; i++){
			String str = (String)array.get(i);
			list.add(str);
		}
		return list;
	}
	
	/*
	 * FIND GAME EXTERNAL EVENT
	 */
	@SuppressWarnings("unchecked")
	public void findGames(){
		//call to server
		List<String> games = null;
		JSONObject jsonOut = new JSONObject();
			
		// Send a FIND_GAME request to server		
		jsonOut.put("Description", "FIND_GAME");
		sendMessage(jsonOut);
		
		// Get the game list from server
		JSONArray inJArray;
		String inMessage;
		try {
			inMessage = (String) in.readLine();
			System.out.println(inMessage);
			// Parse the JSON stream to List<String>
			Map re = toJSON(inMessage.toString());
			if(re.get("Description").toString().equals("LIST_GAME")) {
				games = (List<String>)re.get("Games");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//populate games
		EventFactory.getInstance().makeInternalListGames(games, 0);
		
	}
	/*
	 * CREATE GAME EXTERNAL EVENT
	 */
	@SuppressWarnings("unchecked")
	public void createGame(String gameName) {
		JSONObject jsonOut = new JSONObject();
		String inMessage;

					
		// Send a CREATE_GAME request to server		
		jsonOut.put("Description", "CREATE_GAME");
		jsonOut.put("Game Name", gameName);
		sendMessage(jsonOut);
		
		jsonOut.clear();
		
		// Get the game list from server		
		try {
			inMessage = (String) in.readLine();
			System.out.println(inMessage);
			
			// Parse the JSON stream to List<String>
			Map re = toJSON(inMessage.toString());
			String str = re.get("Description").toString();
			if(!str.equals("GAME_CREATED")) {
				System.out.println("Unable to create game: " + str);
				// 	TODO: populate a error event
				return;
			}
			
			// Populate a internal create game event
			EventFactory.getInstance().makeInternalCreateGame(gameName,0);	// TODO: time?


			// Send a waiting message
			jsonOut.put("Description", "WAITING_FOR_PEER");
			sendMessage(jsonOut);
			
			// Waiting for start game notification from server
			System.out.println("Waiting for start game " +
									"notification from server");
			inMessage = (String) in.readLine();
			System.out.println(inMessage);
			
			re = toJSON(inMessage.toString());
			if(re.get("Description").toString().equals("START_GAME")) {
				String ip = re.get("IP Address").toString();
				EventFactory.getInstance().makeExternalStartGame(ip, gameName, LunarLanderModel.PlayerOne);
				System.out.println("Start game event is populated.");
			}
			
			// TODO: Unexpected message for server
			
			str = re.get("Description").toString();
						
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/*
	 * JOIN GAME EXTERNAL EVENT
	 */
	@SuppressWarnings("unchecked")
	public void joinGame(String gameName) {
		String peerIP = null;
		String inMessage;
		JSONObject jsonOut = new JSONObject();
					
		// Send a JOIN_GAME request to server		
		jsonOut.put("Description", "JOIN_GAME");
		jsonOut.put("Game Name", gameName);
		sendMessage(jsonOut);	
		
		try {
			inMessage = (String) in.readLine();
			System.out.println(inMessage);
			
			// Parse the JSON stream to List<String>
			Map re = toJSON(inMessage.toString());
			String str = re.get("Description").toString();
			if(str.equals("GAME_FULL")) {
				System.out.println("Unable to join game: " + str);
				// TODO: return error event
				return;
			}
			peerIP = re.get("IP Address").toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		EventFactory.getInstance().getInstance().makeExternalStartGame(peerIP, gameName, LunarLanderModel.PlayerTwo);
	}
	
	public void end() {
		// Send a CLOSE_CONNNECTION request to server	
		out.println("CLOSE_CONNECTION");
		String inMessage;
		try {
			inMessage = (String) in.readLine();
			if(inMessage.equals("CLOSE_CONNECTION")) {
				// Close socket
				in.close();
				out.close();
				clientSocket.close();
			} else {
				System.out.print("Unexpected response: " + inMessage);
			}
		} catch (IOException ioException) {
			// TODO Auto-generated catch block
			System.out.println("Socket closing failed.");
			ioException.printStackTrace();
		}

	
	}

	@Override
	public void sendExternalEvent(ExternalEvent externalEvent) {
		// TODO Auto-generated method stub
		
	}

	private void sendMessage(JSONObject obj)
	{
		String msg = obj.toString();
		System.out.println("Sending: " + msg);
		out.println(msg);
	}
	
	public static void main(String args[])
	{
		GameStarter gameStarter = GameStarter.getInstance();
	
		gameStarter.run();
		gameStarter.findGames();
		gameStarter.createGame("BigKobsa");
//		gameStarter.createGame("DigitalPluto");
		gameStarter.end();
	}
	
	@SuppressWarnings("rawtypes")
	private Map toJSON(String inMsg) {
		JSONParser parser = new JSONParser();
		 Map json = null;
		
		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}
			
			public Map createObjectContainer() {
				return new LinkedHashMap();
			}
		};
		
		try{
		    json = (Map)parser.parse(inMsg, containerFactory);
		    /*Iterator iter = json.entrySet().iterator();
		    while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		    }*/
		                        
		  }
		  catch(ParseException pe){
		    System.out.println(pe);
		  }
		
		  return json;
	}

	public String getIPofGameServer() {
		return IPofGameServer;
	}

	public void setIPofGameServer(String iPofGameServer) {
		IPofGameServer = iPofGameServer;
	}
	
}
