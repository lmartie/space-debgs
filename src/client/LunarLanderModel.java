package client;

import java.util.Observable;

import org.eclipse.swt.graphics.Point;

import shapes.DoublePoint;
import shapes.Line;
import shapes.Triangle;

public class LunarLanderModel extends Observable {

	private Triangle body;
	private double speed = 150;
	private double actualSpeed = 0;
	private Line resultant;
	private int playerNumber;

	public static int PlayerOne = 1;
	public static int PlayerTwo = 2;

	private Line ground; 
	private double fuel = 100; 
	private double rotation = 90;
	private static int fireLaserCost = 10;
	private int shotsFired = 0;
	private double altitude = 0;
	private int bugs = 0;
	
	public enum Modules {rocket,speed,fuel,rotation};
	
	public LunarLanderModel(){};
	
	public LunarLanderModel(int player, Line ground) {
		// top of lander
		DoublePoint locationTop = null;
		// left point of lander
		DoublePoint locationLeft = null;
		// right point of lander
		DoublePoint locationRight = null;
		// setting initial position
		if (player == PlayerOne) {
			locationTop = new DoublePoint(45, 90);
			setPlayerNumber(PlayerOne);
		} else if (player == PlayerTwo) {
			locationTop = new DoublePoint(555, 90);
			setPlayerNumber(PlayerTwo);
		}
		locationLeft = new DoublePoint(locationTop.x - 25, locationTop.y + 30);
		locationRight = new DoublePoint(locationTop.x + 25, locationTop.y + 30);
		setBody(new Triangle(locationLeft, locationTop, locationRight));
		resultantVelocity();
		this.setGround(ground);
		this.setAltitude(ground.getTwo().y - body.getBase().getCenter().y);
	}
	
	public void makeLander(int player, Line ground){
		// top of lander
		DoublePoint locationTop = null;
		// left point of lander
		DoublePoint locationLeft = null;
		// right point of lander
		DoublePoint locationRight = null;
		// setting initial position
		if (player == PlayerOne) {
			locationTop = new DoublePoint(45, 90);
			setPlayerNumber(PlayerOne);
		} else if (player == PlayerTwo) {
			locationTop = new DoublePoint(555, 90);
			setPlayerNumber(PlayerTwo);
		}
		locationLeft = new DoublePoint(locationTop.x - 25, locationTop.y + 30);
		locationRight = new DoublePoint(locationTop.x + 25, locationTop.y + 30);
		setBody(new Triangle(locationLeft, locationTop, locationRight));
		resultantVelocity();
		this.setGround(ground);
		this.setAltitude(ground.getTwo().y - body.getBase().getCenter().y);
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
		updateObservers();
	}

	public Triangle getBody() {
		return body;
	}

	public void setBody(Triangle body) {
		this.body = body;
		updateObservers();
	}

	public Line getThrustDirection() {
		double resultRotation = -Math.toRadians(this.rotation);
		
		double A = speed*Math.cos(resultRotation);
		double B = speed*Math.sin(resultRotation);
		
		DoublePoint center = this.body.getBase().getCenter();
		
		double newPerpX = ((A*Math.cos(Math.toRadians(-180))) - (B*Math.sin(Math.toRadians(-180))));
		double newPerpY = ((A*Math.sin(Math.toRadians(-180))) + (B*Math.cos(Math.toRadians(-180))));
		
		Line perp = new Line(new DoublePoint(newPerpX+center.x,newPerpY+center.y),center);
		
		return perp;
	}

	public shapes.Line getGround() {
		return ground;
	}

	public void setGround(Line ground) {
		this.ground = ground;
		updateObservers();
	}

	/**
	 * 
	 * @return true if enough fuel to fire else false
	 */
	public boolean fireLaser() {
		if (fuel >= fireLaserCost) {
			fuel = fuel - fireLaserCost;
			shotsFired++;
			return true;
		} else {
			return false;
		}
	}

	public double getFuel(){
		return fuel;
	}

	public void setFuel(double fuel) { 
		this.fuel = fuel;
		updateObservers();
	}

	public double getRotation() {
		return rotation; 
	}

	public void setRotation(double rotate) {
		this.rotation = rotation+rotate;
		
		if(rotation >= 360){
			rotation = rotation%360;
		}else if(rotation <= -360){
			rotation = rotation%-360;
		}
		
		
		body.rotate(rotate);
		resultantVelocity();
		updateObservers();

	}
	
	public void move(DoublePoint point){
		this.body.moveBy(point);
		resultantVelocity();
		this.setAltitude(ground.getTwo().y - body.getBase().getCenter().y);
		
		consumeFuelOnMove();
		
		updateObservers();
	}
	
	private void consumeFuelOnMove(){
		fuel = fuel - speed*(.1);
	}
	
	private void consumeFuelOnFire(){
		fuel = fuel - fuel*(.1);
	}
	
	public double getConsumptionOnMove(){
		if(speed >=0)
			return speed*(.1);
		else
			return 0;
	}
	public double getConsumptionOnFire(){
		return 10;
	}
	
	public void resultantVelocity(){
		
		double resultRotation = -Math.toRadians(this.rotation);
		
		double resA = speed*Math.cos(resultRotation)+GameModel.gravityForce*Math.cos(-Math.toRadians(270));
		double resB = (speed*Math.sin(resultRotation)+GameModel.gravityForce*Math.sin(-Math.toRadians(270)));
		
		DoublePoint center = this.body.getBase().getCenter();
		setResultant(new Line(center,new DoublePoint(resA+center.x,(resB+center.y))));
		
		this.setActualSpeed(Math.sqrt((Math.pow(resA-10, 2)+Math.pow(resB-10, 2))));
	}

	public void updateObservers() {
		this.setChanged();
		this.notifyObservers();
		this.clearChanged();
	}

	public Line getResultant() {
		return resultant;
	}

	public void setResultant(Line resultant) {
		this.resultant = resultant;
	}

	public double getActualSpeed() {
		return actualSpeed;
	}

	public void setActualSpeed(double actualSpeed) {
		this.actualSpeed = actualSpeed;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
		updateObservers();
	}

	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}

	public int getBugs() {
		return bugs;
	}

	public void setBugs(int bugs) {
		this.bugs = bugs;
		updateObservers();
	}
	
	public void incrementBugs(){
		bugs++;
		updateObservers();
	}
}