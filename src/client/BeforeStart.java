package client;

import org.eclipse.ui.IStartup;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class BeforeStart implements IStartup {

	/**
	*
	* @tag early Startup workbenchwindows iresourcechangeevent pre
	*/

	public void earlyStartup() {
		IResourceChangeListener listener = new EditorListener();
		   ResourcesPlugin.getWorkspace().addResourceChangeListener(listener,
				      IResourceChangeEvent.PRE_CLOSE
				      | IResourceChangeEvent.PRE_DELETE
				      | IResourceChangeEvent.PRE_BUILD
				      | IResourceChangeEvent.POST_BUILD
				      | IResourceChangeEvent.POST_CHANGE);
	}

}

