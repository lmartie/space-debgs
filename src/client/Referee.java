package client;

import java.util.List;
import java.util.Observable;

import events.Event;
import events.EventFactory;
import events.internal.FireLaser;
import events.internal.MoveLander;

public class Referee extends Observable{
	
	private static Referee referee;
	
	private GameModel gameModel;
	private List<String> games;
	private String status = "idle";
	
	private static int minAltitude = 5;
	
	private Referee(){
		
	}
	
	public static Referee getInstance(){
		if(referee == null){
			referee = new Referee();
		}
		return referee;
	}
	
	
	/**
	 * 
	 * @param model
	 * @param event
	 * @return
	 */
	public boolean isLegal(PlayerModel player, Event event){
		LunarLanderModel model = player.getLunarLander();
		
		if(event instanceof MoveLander){			
			double fuel = model.getFuel();
			
			if(0 <= fuel-model.getConsumptionOnMove()  && !player.isTurnOver()){
				if(!player.isUsedMove()){
					return true;
				}else{
					createErrorEvent("You can only move once per turn.");
				}
				
			}else{
				if(player.isTurnOver()){
					createErrorEvent("Your turn is over.");
				}else{
					createErrorEvent("Not enough fuel to move at set speed.");
				}
			}
		}else if (event instanceof FireLaser){
			
			double fuel = model.getFuel();
			
			
			if(0 <= fuel-model.getConsumptionOnFire() && !player.isTurnOver()){
				return true;
			}else if(!player.isTurnOver()){
				createErrorEvent("Not enough fuel to fire laser.");
			}else{
				createErrorEvent("Your turn is over.");
			}
		}
		
		
		
		return false;
	}
	
	private void createErrorEvent(String error){
		EventFactory.getInstance().makeInternalError(error);
	}
	
	
	public boolean isCrashed(LunarLanderModel model){
		double actualSpeed = model.getActualSpeed();
		double altitude = model.getAltitude();
		double rotation = model.getRotation();
		
		if(actualSpeed > 5 && altitude <= minAltitude){
			return true;
		}else if((rotation >= 100 || rotation <=85 ) && altitude <= minAltitude){
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean isLanded(LunarLanderModel model){
		if(!isCrashed(model) && model.getAltitude() <= minAltitude){
			return true;
		}else{
			return false;
		}
	}
	
	public void setGame(GameModel gameModel){
		this.setGameModel(gameModel);
	}
	
	public boolean isGameOver(){
		PlayerModel me = gameModel.getMe();
		PlayerModel you = gameModel.getYou();
		
		LunarLanderModel myLander = me.getLunarLander();
		LunarLanderModel yourLander = you.getLunarLander();
		
		boolean meCrash = isCrashed(myLander);
		boolean youCrash = isCrashed(yourLander);
		
		if(meCrash || youCrash)
			return true;
		
		boolean meLand = isLanded(myLander);
		boolean youLand = isLanded(yourLander);
		
		if(meLand || youLand){
			return true;
		}
		
		return false;
		
	}
	
	public boolean isTie(){
		PlayerModel me = gameModel.getMe();
		PlayerModel you = gameModel.getYou();
		
		LunarLanderModel myLander = me.getLunarLander();
		LunarLanderModel yourLander = you.getLunarLander();
		
		boolean meCrash = isCrashed(myLander);
		boolean youCrash = isCrashed(yourLander);
		
		boolean meLand = isLanded(myLander);
		boolean youLand = isLanded(yourLander);
		
		if(meCrash && youCrash)
			return true;
		if(meLand && youLand){
			return true;
		}
		
		return false;
			
	}
	
	/**
	 * works if no tie, run that first
	 * @return null if no one won
	 */
	public PlayerModel whoWon(){
		PlayerModel model = null;
		
		if(isGameOver() && !isTie()){
			PlayerModel me = gameModel.getMe();
			PlayerModel you = gameModel.getYou();
			
			LunarLanderModel myLander = me.getLunarLander();
			LunarLanderModel yourLander = you.getLunarLander();
			
			boolean meLand = isLanded(myLander);
			boolean youLand = isLanded(yourLander);
			
			boolean meCrash = isCrashed(myLander);
			boolean youCrash = isCrashed(yourLander);
			
			if(meLand)
				return me;
			else if(youLand)
				return you;
			else if(meCrash)
				return you;
			else if(youCrash)
				return me;
		}
		
		return model;
	}
	
	public void startGame(){
		Timer.getInstance().stop();
		Timer.getInstance().start();
	}

	public GameModel getGameModel() {
		return gameModel;
	}

	public void setGameModel(GameModel gameModel) {
		this.gameModel = gameModel;
	}

	public List<String> getGames() {
		return games;
	}

	public void setGames(List<String> games) {
		this.games = games;
		updateObservers();
	}
	
	public void updateObservers() {
		this.setChanged();
		this.notifyObservers();
		this.clearChanged();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		updateObservers();
	}

}
