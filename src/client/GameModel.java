package client;

public class GameModel {
	
	private PlayerModel me;
	private PlayerModel you;
	private int turn = 0;
	private String gameName = null;
	private String ipOfOpponent = null;
	//v = v0 + at where a = 1.622 m/s^2
	public static double gravityForce = 97.32;
	
	public GameModel(PlayerModel me, PlayerModel you, String name, String ipOfOpponent){
		this.setMe(me);
		this.setYou(you);
		this.setGameName(name);
		this.setIpOfOpponent(ipOfOpponent);
	}

	public PlayerModel getMe() {
		return me;
	}

	public void setMe(PlayerModel me) {
		this.me = me;
	}

	public PlayerModel getYou() {
		return you;
	}

	public void setYou(PlayerModel you) {
		this.you = you;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getIpOfOpponent() {
		return ipOfOpponent;
	}

	public void setIpOfOpponent(String ipOfOpponent) {
		this.ipOfOpponent = ipOfOpponent;
	}

	
	

}
