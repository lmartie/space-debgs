package client;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import events.Event;
import events.EventFactory;

/**
 * 
 * @author lee
 * 
 */
public class EventOrganizer implements Observer {
	private int time = 0;

	private static EventOrganizer eventOrganizer = null;
	private static int endtime = 6000;
	
	private EventOrganizer() {

	}

	public static EventOrganizer getInstance() {
		if (eventOrganizer == null) {
			eventOrganizer = new EventOrganizer();
		}
		return eventOrganizer;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof client.Timer) {
			time = (int) arg;
		//	System.out.println("[EventOrganizer] Time Update "+time);
			//get games automatically...
			//EventFactory.getInstance().makeExternalFindGames(time);

			//get events from peer
			if(PeerCommunicating_Client.getInstance().isConnected()){
				System.out.println("*********************************CONNECTED TO PEER'S SERVER******************************************");
				PeerCommunicator.getInstance().sendGetEvent(time);
			}
			
			List<Event> events = EventCollection.getInstance()
					.getAndRemoveEventsBeforeAndOnTime(time);
			
			//System.out.println("[EventOrganizer] got events "+events);
			
			
			EventProcessor.getInstance().processEvents(events);
			
			if(time >= endtime && Referee.getInstance().getGameModel() != null){
				EventFactory.getInstance().makeInternalEndTurn(time);
			}
		}

	}

}
