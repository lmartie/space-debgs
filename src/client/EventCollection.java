package client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

import events.Event;
import events.external.ExternalEvent;
import events.internal.InternalEvent;

/**
 * keeps events sorted by time
 * 
 * singleton class
 * 
 * @author lee
 * 
 */
public class EventCollection{

	private HashMap<Integer, List<Event>> eventTable = new HashMap<Integer, List<Event>>();

	
	private static EventCollection eventCollection = null;
	
	private EventCollection (){
		
	}
	
	public static EventCollection getInstance(){
		if(eventCollection == null){
			eventCollection = new EventCollection();
		}
		
		return eventCollection;
	}
	
	
	/**
	 * adds event to collection in ascending sorted order of time stamps
	 * @param event
	 */
	public void addEvent(Event event) {
		int time = event.getTimeStamp();

		if (eventTable.get(time) == null) {
			List<Event> events = new ArrayList<Event>();
			eventTable.put(time, events);
			events.add(event);
		} else {
			List<Event> events = eventTable.get(time);

			Collections.sort(events, new Comparator<Event>() {
				public int compare(Event o1, Event o2) {

					int time1 = o1.getTimeStamp();
					int time2 = o2.getTimeStamp();

					if (time1 > time2)
						return 1;
					else if (time1 < time2)
						return -1;
					else
						return 0;

				}
			});

			
			events.add(event);	
		}
		
		System.out.println("[EventsCollection] Event added to Events Collection");
		
	}
	
	public List<Event> getAndRemoveEventsBeforeAndOnTime(int time){
		List<Event> events = getEventsBeforeAndOnTime(time);
		removeEventsBeforeAndOnTime(time);
		return events;
	}

	/**
	 * removes all events before and on the time given
	 * @param time
	 */
	public void removeEventsBeforeAndOnTime(int time) {
	//	System.out.println("[EventsCollection] Removing events <= "+time);
		for (int i = 0; i <= time; i++) {
			eventTable.remove(i);
		}
	}
	/**
	 * returns all events before and on the time given
	 * @param time
	 */
	public List<Event> getEventsBeforeAndOnTime(int time) {
		//System.out.println("[EventsCollection] Getting events <= "+time);
		ArrayList<Event> events = new ArrayList<Event>();
		for (int i = 0; i <= time; i++) {
			if(eventTable.get(i) != null)
				events.addAll(eventTable.get(i));
		}
		
		return events;
	}

}
