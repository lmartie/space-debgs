package client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import events.Event;
import events.external.ExternalEvent;
import events.external.peer.PeerEvent;

public class ExternalEventCollection {

private HashMap<Integer, List<PeerEvent>> eventTable = new HashMap<Integer, List<PeerEvent>>();


private static ExternalEventCollection eventCollection = null;

private ExternalEventCollection (){
	
}

public static ExternalEventCollection getInstance(){
	if(eventCollection == null){
		eventCollection = new ExternalEventCollection();
	}
	
	return eventCollection;
}


/**
 * adds event to collection in ascending sorted order of time stamps
 * @param event
 */
public void addEvent(PeerEvent event) {
	int time = event.getTimeStamp();

	if (eventTable.get(time) == null) {
		List<PeerEvent> events = new ArrayList<PeerEvent>();
		eventTable.put(time, events);
		events.add(event);
	} else {
		List<PeerEvent> events = eventTable.get(time);

		Collections.sort(events, new Comparator<PeerEvent>() {
			public int compare(PeerEvent o1, PeerEvent o2) {

				int time1 = o1.getTimeStamp();
				int time2 = o2.getTimeStamp();

				if (time1 > time2)
					return 1;
				else if (time1 < time2)
					return -1;
				else
					return 0;

			}
		});

		
		events.add(event);	
	}
	
	System.out.println("[EventsCollection] Event added to Events Collection");
	
}

public List<PeerEvent> getAndRemoveEventsBeforeAndOnTime(int time){
	List<PeerEvent> events = getEventsBeforeAndOnTime(time);
	removeEventsBeforeAndOnTime(time);
	return events;
}

/**
 * removes all events before and on the time given
 * @param time
 */
public void removeEventsBeforeAndOnTime(int time) {
	System.out.println("[EventsCollection] Removing events <= "+time);
	for (int i = 0; i <= time; i++) {
		eventTable.remove(i);
	}
}
/**
 * returns all events before and on the time given
 * @param time
 */
public List<PeerEvent> getEventsBeforeAndOnTime(int time) {
	System.out.println("[EventsCollection] Getting events <= "+time);
	ArrayList<PeerEvent> events = new ArrayList<PeerEvent>();
	for (int i = 0; i <= time; i++) {
		if(eventTable.get(i) != null)
			events.addAll(eventTable.get(i));
	}
	
	return events;
}
}

