package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import events.external.ExternalEvent;

/**
 * 
 * @author hsiaohsy 
 * USEAGE: 
 * 1) call PeerCommunicator.getInstance().connect(peerIPAddress)
 *
 */
public class PeerCommunicator implements IPeerCommunicator{
	private static PeerCommunicator peerCommunicator = null;

	private PeerCommunicator() {
		Thread t_serv = new Thread(PeerCommunicating_Server.getInstance());
		t_serv.start();
	}

	public static PeerCommunicator getInstance() {
		if (peerCommunicator == null) {
			peerCommunicator = new PeerCommunicator();
		}
		return peerCommunicator;
	}
	
	
	public void connect(String peer_ip) {
		PeerCommunicating_Client.getInstance().connectToServer(peer_ip);
	}
	
	public void sendGetEvent(int time)
	{
		PeerCommunicating_Client.getInstance().getEventsFromServer(time);	
	}
	
	public void close(){
		// PeerCommunicating_Client disconnect to server
		PeerCommunicating_Client.getInstance().close();
	}
	
//	@Override
//	public void sendExternalEvent(ExternalEvent externalEvent) {
//		// TODO Auto-generated method stub
//		
//	}

}
