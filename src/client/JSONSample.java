package client;
import events.external.ExternalEvent;
import events.external.peer.PeerEvent;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JSONSample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String jsonString = "[{\"timeStamp\":\"680\",\"Modules\":speed,\"Type\":\"LaserHit\"}]";
		System.out.println("Input: " + jsonString);
		List<PeerEvent> events = new LinkedList<PeerEvent>();
		
		JSONParser parser = new JSONParser();
		
		Object obj;
		try {
			obj = parser.parse(jsonString);
			JSONArray array = (JSONArray) obj;
			System.out.println(array.get(0));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
//		for(int i=0; i<array.size(); i++) {
//			Map m = (Map) array.get(i);
//			System.out.println("i");
////			events.add(toPeerEvent(m));			
//		}
		
//		List<String> games = null;
		
//		Map re = toJSON(str.toString());
//		if(re.get("Description").toString().equals("LIST_GAME")) {
//			games = (List<String>)re.get("Games");
//		}
//		
//		for(String s: games) {
//			System.out.println(s);
//		}
		

	}
	
	private static Map toJSON(String inMsg) {
		JSONParser parser = new JSONParser();
		 Map json = null;
		
		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}
			
			public Map createObjectContainer() {
				return new LinkedHashMap();
			}
		};
		
		try{
		    json = (Map)parser.parse(inMsg, containerFactory);
		    /*Iterator iter = json.entrySet().iterator();
		    while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		    }*/
		                        
		  }
		  catch(ParseException pe){
		    System.out.println(pe);
		  }
		
		  return json;
	}

}

