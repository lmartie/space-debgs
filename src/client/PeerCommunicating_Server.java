package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import events.external.peer.PeerEvent;

public class PeerCommunicating_Server implements Runnable{

	private static PeerCommunicating_Server server = null;
	private static ServerSocket serverSocket = null;
	private static int portNum = 2005;
	private static Socket clientSocket;
	private String client_ip;
	
	BufferedReader in = null;
	PrintWriter out = null;
	
	public static PeerCommunicating_Server getInstance() {
		if (server == null) {
			server = new PeerCommunicating_Server();
			/*
			 * Open a server socket on the portNum
			 */
			try {
				serverSocket = new ServerSocket(portNum);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Could not listen on port" + portNum);
			    System.exit(-1);
			}
		}
		return server;
	}
	
	private void close() {
		try {
			// Close socket
			in.close();
			out.close();
			clientSocket.close();

		} catch (IOException ioException) {
			// TODO Auto-generated catch block
			System.out.println("Socket closing failed.");
			ioException.printStackTrace();
		}
		System.out.println("Client socket closed!");
	}

	@Override
	public void run() {

	
		
		try {
			clientSocket = serverSocket.accept();
		} catch (IOException e) {
			System.out.println("Accept failed.");
		    System.exit(-1);
			
		}
		
		String client_ip = clientSocket.getInetAddress().getHostAddress();
		System.out.println("Connection received from " + client_ip);
		
		try {
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new PrintWriter(clientSocket.getOutputStream(), true);	
			
		} catch (IOException e) {
			System.out.println("in or out failed");
			System.exit(-1);
		}
		
		String inMessage;
		while(true) {
			try {
				LinkedList eventList = new LinkedList();
				inMessage = (String) in.readLine();
				System.out.println(inMessage + " from " + client_ip);
				
				if(inMessage.equals("CLOSE_CONNECTION")) {
					break;
				}
								
				// Parse the JSON stream to List<String>
				Map re = toJSON(inMessage);
				
				if(re.get("Description").toString().equals("GET_EVENTS")) {		
					int time = Integer.parseInt(re.get("Time").toString());					
					eventList = getPeerEvents(time);					
					// Send the event to client
					sendMessage(eventList);					
				} else {
					// TODO: Close connection?
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		// Close server socket
		close();
	}
	
	private LinkedList getPeerEvents(int time) {
		LinkedList events = new LinkedList();
		
		// Get events 
		List<PeerEvent> peerEvents = 
				ExternalEventCollection.getInstance().getAndRemoveEventsBeforeAndOnTime(time);
		
		// Add the events to LinkedList
		for(int i = 0; i < peerEvents.size(); i++) {
			Map m = peerEvents.get(i).toHashMap();
			events.add(m);
		}
		
		return events;
	}
	
	@SuppressWarnings("unused")
	private void sendMessage(LinkedList list)
	{
		String msg = JSONValue.toJSONString(list);
		System.out.println("Sending: " + msg);
		out.println(msg);
	}
	
	@SuppressWarnings("unused")
	private void sendMessage(JSONObject obj)
	{
		String msg = obj.toString();
		System.out.println("Sending: " + msg);
		out.println(msg);
	}
	
	@SuppressWarnings("rawtypes")
	private Map toJSON(String inMsg) {
		JSONParser parser = new JSONParser();
		 Map json = null;
		
		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}
			
			public Map createObjectContainer() {
				return new LinkedHashMap();
			}
		};
		
		try{
		    json = (Map)parser.parse(inMsg, containerFactory);
		    /*Iterator iter = json.entrySet().iterator();
		    while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		    }*/
		                        
		  }
		  catch(ParseException pe){
		    System.out.println(pe);
		  }
		
		  return json;
	}
	
	
}
