package client;

import java.util.Observable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

public class Timer extends Observable{
	
	private static Timer timerInstance = null;
	private static boolean stop = false;
	
	private int time = 0;
	private int restarts = 0;
	
	private Timer(){
		
	}
	
	public static Timer getInstance(){
		if(timerInstance == null){
			timerInstance = new Timer();
		}
		return timerInstance;
	}
	
	public void start(){
		time = 0;
		stop = false;
		Job job = new Job("Timer Job") {
		     protected IStatus run(IProgressMonitor monitor) {
		    	   Timer.getInstance().run();
		           return Status.OK_STATUS;
		        }
		     };
		  job.setPriority(Job.SHORT);
		  job.schedule(); // start as soon as possible
		  
		 
	}
	
	public void restart(){
//		stop = false;
		time = 0;
//		restarts++;
//		Job job = new Job("Timer Job") {
//		     protected IStatus run(IProgressMonitor monitor) {
//		    	   this.run(monitor);
//		    	   return Status.OK_STATUS;
//		        }
//		     };
//		  job.setPriority(Job.SHORT);
//		  job.schedule(); // start as soon as possible
		  
	}
	
	public void stop(){
		stop = true;
	}

	public void run() {
			
			while(!stop){
			 this.setChanged();
			 this.notifyObservers(time);
			 this.clearChanged();
			 
				try {
					 
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				time++;
				this.run();
			}
	}
	

}
