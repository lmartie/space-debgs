package client;

import imageview.views.GameView;

import java.util.List;
import java.util.Random;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

import bugs.BugGenerator;

import shapes.DoublePoint;
import shapes.Line;

import events.Event;
import events.external.CreateGame;
import events.external.ExternalEvent;
import events.external.FindGame;
import events.external.GameServerListRefresh;
import events.external.JoinGame;
import events.external.ListOfGames;
import events.external.StartGame;
import events.external.peer.CoordinatesChange;
import events.external.peer.ExternalEndTurn;
import events.external.peer.ExternalGameOver;
import events.external.peer.LaserHit;
import events.external.peer.ExternalSpeedRotationChange;
import events.internal.ChangeStatus;
import events.internal.EndTurn;
import events.internal.FireLaser;
import events.internal.InternalEvent;
import events.internal.InternalLLError;
import events.internal.MoveLander;
import events.internal.SpeedRotationChange;
import events.internal.StartTurn;

public class EventProcessor {

	private static EventProcessor eventProcessor = null;

	private EventProcessor() {

	}

	public static EventProcessor getInstance() {
		if (eventProcessor == null) {
			eventProcessor = new EventProcessor();
		}
		return eventProcessor;
	}

	private void displayNotification(final String notice){
		final Display display = PlatformUI.getWorkbench().getDisplay();
		display.asyncExec((new Runnable() {
			public void run() {
				MessageBox messageBox = new MessageBox(display
						.getActiveShell(), SWT.ICON_INFORMATION);
				messageBox.setMessage(notice);
				int rc = messageBox.open();
			}
		}));
	}
	
	private void determineWinner(Event event){
		Referee.getInstance().setStatus("Game Over!");
		
		if(Referee.getInstance().isTie()){
			ExternalGameOver externalEndGame = new ExternalGameOver(ExternalGameOver.OpponentStatus.tie);
			externalEndGame.setTimeStamp(event.getTimeStamp());
			ExternalEventCollection.getInstance().addEvent(externalEndGame);
			
		}else{
			PlayerModel playerWon = Referee.getInstance().whoWon();
			int playerNumberWon = playerWon.getLunarLander().getPlayerNumber();
			
			if(playerNumberWon == Referee.getInstance().getGameModel().getYou().getLunarLander().getPlayerNumber()){
				ExternalGameOver externalEndGame = new ExternalGameOver(ExternalGameOver.OpponentStatus.win);
				externalEndGame.setTimeStamp(event.getTimeStamp());
				ExternalEventCollection.getInstance().addEvent(externalEndGame);
				
			}else{
				ExternalGameOver externalEndGame = new ExternalGameOver(ExternalGameOver.OpponentStatus.lose);
				externalEndGame.setTimeStamp(event.getTimeStamp());
				ExternalEventCollection.getInstance().addEvent(externalEndGame);
				
			}
		}
	}
	
	private void newTurn(){
		Referee.getInstance().getGameModel().getMe().setTurnOver(false);
		Referee.getInstance().getGameModel().getMe().setUsedMove(false);
		Referee.getInstance().getGameModel().getYou().setTurnOver(false);
		Referee.getInstance().getGameModel().getYou().setUsedMove(false);
		Timer.getInstance().restart();
		Referee.getInstance().setStatus("Turn Started");
	}
	
	/**
	 * Assumes events are ordered
	 */
	public void processEvents(List<Event> events) {

		for (Event event : events) {
			// SPEED ROTATION CHANGE
			if (event instanceof SpeedRotationChange) {
				
				System.out.println("[EventProcessor] Internal ..." + event);
				SpeedRotationChange speedRotationChange = (SpeedRotationChange) event;
				double speed = speedRotationChange.getSpeed();
				double rotation = speedRotationChange.getRotation();
				
				//put on external even collection
				ExternalSpeedRotationChange rotationChange = new ExternalSpeedRotationChange(rotation,speed);
				rotationChange.setTimeStamp(event.getTimeStamp());
				ExternalEventCollection.getInstance().addEvent(rotationChange);
				


				Referee.getInstance().getGameModel().getMe().getLunarLander()
						.setSpeed(speed);
				Referee.getInstance().getGameModel().getMe().getLunarLander()
						.setRotation(rotation);


				// MoveLander
			} else if (event instanceof MoveLander) {

				boolean legal = Referee.getInstance().isLegal(
						Referee.getInstance().getGameModel().getMe(), event);

				if (legal) {
					MoveLander moveLander = (MoveLander) event;
					Line resultant = Referee.getInstance().getGameModel()
							.getMe().getLunarLander().getResultant();

					double centerX = Referee.getInstance().getGameModel()
							.getMe().getLunarLander().getBody().getBase()
							.getCenter().x;
					double centerY = Referee.getInstance().getGameModel()
							.getMe().getLunarLander().getBody().getBase()
							.getCenter().y;

					double newX = resultant.getTwo().x - centerX;
					double newY = resultant.getTwo().y - centerY;
					
					DoublePoint newPoint = new DoublePoint(newX, newY);
					
					//put on external even collection
					CoordinatesChange coordinatesChange = 
						new CoordinatesChange(newPoint.asSWTPoint().x,newPoint.asSWTPoint().y);
					coordinatesChange.setTimeStamp(event.getTimeStamp());
					ExternalEventCollection.getInstance().addEvent(coordinatesChange);
					
					
					Referee.getInstance().getGameModel().getMe()
							.getLunarLander().move(newPoint);
					
					
					
					Referee.getInstance().getGameModel().getMe()
							.setUsedMove(true);
				}
			}// Fire Laser
			else if (event instanceof FireLaser) {
				boolean legal = Referee.getInstance().isLegal(
						Referee.getInstance().getGameModel().getMe(), event);

				if (legal) {
					FireLaser fireLaser = (FireLaser) event;



					boolean shotLaser = Referee.getInstance().getGameModel()
							.getMe().getLunarLander().fireLaser();

					
						System.out.println("[EventProcessor] laser shot");
						Random rand = new Random();
						int hit = rand.nextInt(1);
						
							System.out
									.println("[EventProcessor] laser hit opponent"
											+ hit);
							int module = rand.nextInt(4);
							if (module == 0) {
								System.out
										.println("[EventProcessor] laser hit rotation module"
												+ module);
								LaserHit laserHit = new LaserHit(LunarLanderModel.Modules.rotation);
								laserHit.setTimeStamp(event.getTimeStamp());
								ExternalEventCollection.getInstance().addEvent(laserHit);
								// put bug lunar lander
							} else if (module == 1) {
								System.out
										.println("[EventProcessor] laser hit rocket module"
												+ module);
								LaserHit laserHit = new LaserHit(LunarLanderModel.Modules.rocket);
								laserHit.setTimeStamp(event.getTimeStamp());
								ExternalEventCollection.getInstance().addEvent(laserHit);
							} else if (module == 2) {
								System.out
										.println("[EventProcessor] laser hit fuel module"
												+ module);
								LaserHit laserHit = new LaserHit(LunarLanderModel.Modules.fuel);
								laserHit.setTimeStamp(event.getTimeStamp());
								ExternalEventCollection.getInstance().addEvent(laserHit);
							} else if (module == 3) {
								System.out
										.println("[EventProcessor] laser hit speed module"
												+ module);
								LaserHit laserHit = new LaserHit(LunarLanderModel.Modules.speed);
								laserHit.setTimeStamp(event.getTimeStamp());
								ExternalEventCollection.getInstance().addEvent(laserHit);
							}
						
					

				}

				// START GAME
			} else if (event instanceof StartGame) {
				
				
				StartGame startGameEvent = (StartGame) event;
				System.out.println("[EventProcessor] External ..." + event);

				// start game logic
				String gameName = startGameEvent.getGameName();
				String ipOfOpponent = startGameEvent.getIP();
				int playerNumber = startGameEvent.getPlayerNumber();

				PeerCommunicator.getInstance().connect(ipOfOpponent);

				
				Line ground = new Line(new DoublePoint(0, 600),
						new DoublePoint(600, 600));

				int myNumber;
				int yourNumber;

				if (playerNumber == 1) {
					myNumber = LunarLanderModel.PlayerOne;
					yourNumber = LunarLanderModel.PlayerTwo;
				} else {
					myNumber = LunarLanderModel.PlayerTwo;
					yourNumber = LunarLanderModel.PlayerOne;

				}

				LunarLanderModel myLander = new LunarLanderModel(myNumber,
						ground);
				// binding model and view
				myLander.addObserver(GameView.llvMe);
				myLander.addObserver(GameView.speedLabel);
				myLander.addObserver(GameView.rotationLabel);
				myLander.addObserver(GameView.fuelLabel);
				myLander.addObserver(GameView.newBugLabel);
				myLander.addObserver(GameView.actualSpeedLabel);
				myLander.addObserver(GameView.altitudeLabel);

				PlayerModel me = new PlayerModel(myLander);
				myLander.updateObservers();

				LunarLanderModel yourLander = new LunarLanderModel(yourNumber,
						ground);
				// binding model and view
				yourLander.addObserver(GameView.llvYou);
				PlayerModel you = new PlayerModel(yourLander);
				yourLander.updateObservers();

				GameModel gameModel = new GameModel(me, you, gameName,
						ipOfOpponent);
				Referee.getInstance().setGame(gameModel);
				Referee.getInstance().setStatus("connected to "+ipOfOpponent);
				Timer.getInstance().restart();
				Referee.getInstance().setStatus("Turn Started");
			}
			// List of Games
			else if (event instanceof ListOfGames) {
				System.out.println("[EventProcessor] ListOfGames ..." + event);
				ListOfGames listOfGames = (ListOfGames) event;
				List<String> games = listOfGames.getGames();

				Referee.getInstance().setGames(games);
			}
			// Internal Error
			else if (event instanceof InternalLLError) {
				final InternalLLError error = (InternalLLError) event;

				final Display display = PlatformUI.getWorkbench().getDisplay();
				display.asyncExec((new Runnable() {
					public void run() {
						MessageBox messageBox = new MessageBox(display
								.getActiveShell(), SWT.ICON_ERROR);
						messageBox.setMessage(error.getErrorMsg());
						int rc = messageBox.open();
					}
				}));

			}
			//Join Game
			else if(event instanceof JoinGame){
				JoinGame joinGame = (JoinGame)event;
				GameStarter.getInstance().joinGame(joinGame.getGameName());
			}
			// Create Game
			else if (event instanceof CreateGame) {
				final CreateGame createGame = (CreateGame) event;
				Job job = new Job("GameStarter Job") {
					protected IStatus run(IProgressMonitor monitor) {
						
						GameStarter.getInstance().createGame(
								createGame.getGameName());

						return Status.OK_STATUS;
					}
				};
				job.setPriority(Job.LONG);
				job.schedule(); // start as soon as possible

			}
			// Find Game
			else if (event instanceof FindGame) {
				FindGame findGameEvent = (FindGame) event;

				Job job = new Job("GameStarter Find Job") {
					protected IStatus run(IProgressMonitor monitor) {

						GameStarter.getInstance().findGames();

						return Status.OK_STATUS;
					}
				};
				job.setPriority(Job.SHORT);
				job.schedule(); // start as soon as possible
			}
			// Game server list refresh
			else if (event instanceof GameServerListRefresh) {
				GameServerListRefresh refresh = (GameServerListRefresh) event;
				Job job = new Job("GameStarter Find Job") {
					protected IStatus run(IProgressMonitor monitor) {

						GameStarter.getInstance().findGames();

						return Status.OK_STATUS;
					}
				};
				job.setPriority(Job.SHORT);
				job.schedule(); // start as soon as possible
			}//START TURN 
			else if (event instanceof StartTurn) {
				StartTurn startTurn = (StartTurn) event;
				Referee.getInstance().getGameModel().getMe().setTurnOver(false);
				Timer.getInstance().restart();
			}//END TURN
			else if (event instanceof EndTurn) {
				EndTurn endTurn = (EndTurn) event;
				Referee.getInstance().getGameModel().getMe().setTurnOver(true);
				
				ExternalEndTurn externalEndTurn = new ExternalEndTurn();
				externalEndTurn.setTimeStamp(event.getTimeStamp());
				ExternalEventCollection.getInstance().addEvent(externalEndTurn);
				
				Referee.getInstance().setStatus("Turn Ended");
				
				if(Referee.getInstance().getGameModel().getYou().isTurnOver()){		
					if(Referee.getInstance().isGameOver()){
						determineWinner(event);
						
					}else{
						newTurn();
						
					}
				}
				
			} //CHANGE STATUS 
			else if(event instanceof ChangeStatus){
				ChangeStatus changeStatus = (ChangeStatus)event;
					Referee.getInstance().setStatus(changeStatus.getStatus());
			}//Peer CoordinatesChange
			else if(event instanceof CoordinatesChange){
				CoordinatesChange cordChange = (CoordinatesChange)event;
				
				DoublePoint dp = new DoublePoint(cordChange.x,cordChange.y);
				Referee.getInstance().getGameModel().getYou().getLunarLander().move(dp);
			}
			//Peer RotationChange
			else if(event instanceof ExternalSpeedRotationChange){
				ExternalSpeedRotationChange rotationChange = (ExternalSpeedRotationChange)event;
				Referee.getInstance().getGameModel().getYou().getLunarLander().setRotation(rotationChange.getRotation());
				Referee.getInstance().getGameModel().getYou().getLunarLander().setSpeed(rotationChange.getSpeed());
			}
			//Peer LaserHit
			else if(event instanceof LaserHit){
				LaserHit laserHit = (LaserHit)event;
				Referee.getInstance().getGameModel().getMe().getLunarLander().incrementBugs();
				try {
					if(laserHit.getHitModule() == LunarLanderModel.Modules.fuel){
						BugGenerator.getInstance().generateBug("fuel");
					}else if(laserHit.getHitModule() == LunarLanderModel.Modules.rocket){
						BugGenerator.getInstance().generateBug("rocket");
					}else if(laserHit.getHitModule() == LunarLanderModel.Modules.rotation){
						BugGenerator.getInstance().generateBug("rotation");
					}else if(laserHit.getHitModule() == LunarLanderModel.Modules.speed){
						BugGenerator.getInstance().generateBug("speed");
					}
					
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//Peer EndTurn
			else if(event instanceof ExternalEndTurn){
				ExternalEndTurn extEndTurn = (ExternalEndTurn)event;
				Referee.getInstance().getGameModel().getYou().setTurnOver(true);
				Referee.getInstance().setStatus("Opponent Ended Turn");
				
				if(Referee.getInstance().getGameModel().getMe().isTurnOver()){
					if(Referee.getInstance().isGameOver()){
						determineWinner(event);
						
					}else{
						
						newTurn();
					}
				}
			}
			//Peer End GAME
			else if(event instanceof ExternalGameOver){
				ExternalGameOver gameOver = (ExternalGameOver)event;
			
				if(gameOver.getOpponentStatus() == ExternalGameOver.OpponentStatus.win){
					displayNotification("You Win!!!");
				}else if(gameOver.getOpponentStatus() == ExternalGameOver.OpponentStatus.lose){
					displayNotification("You Lose!!!");
				}else if(gameOver.getOpponentStatus() == ExternalGameOver.OpponentStatus.tie){
					displayNotification("You Tie!!!");
				}
			
				determineWinner(event);
				
				Timer.getInstance().stop();
			}

		}
	}

}
