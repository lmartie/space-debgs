package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import client.LunarLanderModel.Modules;

import events.Event;
import events.external.peer.CoordinatesChange;
import events.external.peer.ExternalEndTurn;
import events.external.peer.ExternalGameOver;
import events.external.peer.LaserHit;
import events.external.peer.PeerEvent;
import events.external.peer.ExternalSpeedRotationChange;

public class PeerCommunicating_Client {
	
	private static PeerCommunicating_Client client = null;
	// The client socket
	private static Socket clientSocket = null;
	// The output stream
	private PrintWriter out;
	// The input stream
	private BufferedReader in;
	// The port number
	private int portNum = 2005;
	private static String server_ip;
	private boolean connected = false;
	
	public static PeerCommunicating_Client getInstance() {
		if (client == null) {
			client = new PeerCommunicating_Client();
			server_ip = null;
		}
		return client;
	}
	
	public void connectToServer(String ip) {
		server_ip = ip;
		try{
			//1. creating a socket to connect to the server
			clientSocket = new Socket(server_ip, portNum);
			System.out.println("Connected to " + server_ip + 
							" in port " + portNum);
			
			//2. get Input and Output streams
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(
					new InputStreamReader(clientSocket.getInputStream()));
			setConnected(true);
		}
		catch(UnknownHostException unknownHost){
			System.err.println("You are trying to " +
						"connect to an unknown host!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	
	public void close() {
		// Send a CLOSE_CONNNECTION request to server	
		out.println("CLOSE_CONNECTION");
		String inMessage;
		try {
			// Close socket
			in.close();
			out.close();
			clientSocket.close();
			
		} catch (IOException ioException) {
			// TODO Auto-generated catch block
			System.out.println("Socket closing failed.");
			ioException.printStackTrace();
		}
		System.out.println("Client socket closed!");
	}

	public void getEventsFromServer(int time) {
		JSONObject jsonOut = new JSONObject();
		
		String inMessage;
		
		// Send a GET_EVENT_REQUEST
		jsonOut.put("Description", "GET_EVENTS");	
		jsonOut.put("Time", Integer.toString(time));		
		sendMessage(jsonOut);
		
		try {
			inMessage = (String) in.readLine();
			System.out.println(inMessage);
			List<PeerEvent> events = parseToEventList(inMessage);
			
			for(Event event: events) {
				// Populate the event to EventCollection
				EventCollection.getInstance().addEvent(event);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private List<PeerEvent> parseToEventList(String jsonString) {
		List<PeerEvent> events = new LinkedList<PeerEvent>();
		
		Object obj = JSONValue.parse(jsonString);
		JSONArray array = (JSONArray) obj;
		
		for(int i=0; i<array.size(); i++) {
			Map m = (Map) array.get(i);
			events.add(toPeerEvent(m));			
		}
		
		return events;
	}
	
	private PeerEvent toPeerEvent(Map m) {
		PeerEvent event = null;
		
		String type = (String) m.get("Type");
		int timestamp = Integer.parseInt(m.get("timeStamp").toString());
		
		if(type.equals("CoordinatesChange")) {
			int x = Integer.parseInt(m.get("x").toString());
			int y = Integer.parseInt(m.get("y").toString());
			event = new CoordinatesChange(x, y);
		} else if (type.equals("ExternalEndTurn")) {
			event = new ExternalEndTurn();
			// TODO: No value to pass here.
		} else if (type.equals("LaserHit")) {
			event = new LaserHit(LaserHit.stringToModule(m.get("Modules").toString()));
		} else if (type.equals("ExternalSpeedRotationChange")) {
			double rotation = Double.parseDouble(m.get("rotation").toString());
			double speed = Double.parseDouble(m.get("speed").toString());
			event = new ExternalSpeedRotationChange(rotation, speed);
		} else if (type.equals("ExternalGameOver")) {
			event = new ExternalGameOver(
					ExternalGameOver.stringToOpponentStatus(m.get("opponentStatus").toString()));
		} else {
		
			System.out.printf("Unknown type of event");
		}
		event.setTimeStamp(timestamp);
		return event;
	}
	
	
	@SuppressWarnings("unused")
	private void sendMessage(JSONObject obj)
	{
		String msg = obj.toString();
		System.out.println("Sending: " + msg);
		out.println(msg);
	}
	
	@SuppressWarnings("rawtypes")
	private Map toJSON(String inMsg) {
		JSONParser parser = new JSONParser();
		 Map json = null;
		
		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}
			
			public Map createObjectContainer() {
				return new LinkedHashMap();
			}
		};
		
		try{
		    json = (Map)parser.parse(inMsg, containerFactory);
		    /*Iterator iter = json.entrySet().iterator();
		    while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		    }*/
		                        
		  }
		  catch(ParseException pe){
		    System.out.println(pe);
		  }
		
		  return json;
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	

}
