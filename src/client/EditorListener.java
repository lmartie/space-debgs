package client;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

public class EditorListener implements IResourceChangeListener {
	 

		/**
		 * @tag resource changed eclipse
		 */
		public void resourceChanged(final IResourceChangeEvent event) {
			IResource res = event.getResource();
			switch (event.getType()) {
			case IResourceChangeEvent.PRE_CLOSE:		
				break;
			case IResourceChangeEvent.PRE_DELETE:
				break;
			case IResourceChangeEvent.POST_CHANGE:
				
				Referee.getInstance().getGameModel().getMe().getLunarLander().updateObservers();

				break;
			case IResourceChangeEvent.PRE_BUILD:
			
			case IResourceChangeEvent.POST_BUILD:
				
				break;
			}

			
		}

	}
