package client;

import events.external.ExternalEvent;

public interface IGameStarter {

	public void sendExternalEvent(ExternalEvent externalEvent);
	
}
