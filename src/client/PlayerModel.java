package client;

public class PlayerModel {
	
	private LunarLanderModel lunarLander;
	private boolean usedMove = false;
	private boolean turnOver = false;
	
	public PlayerModel(LunarLanderModel lunarLander){
		this.setLunarLander(lunarLander);
	}

	public LunarLanderModel getLunarLander() {
		return lunarLander;
	}

	public void setLunarLander(LunarLanderModel lunarLander) {
		this.lunarLander = lunarLander;
	}

	public boolean isUsedMove() {
		return usedMove;
	}

	public void setUsedMove(boolean usedMove) {
		this.usedMove = usedMove;
	}

	public boolean isTurnOver() {
		return turnOver;
	}

	public void setTurnOver(boolean turnOver) {
		this.turnOver = turnOver;
	}

}
